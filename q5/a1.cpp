#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sstream>
#include <string>
#include <sys/time.h>
#include <string.h>

using namespace std;

Display* display;
Window window;
// save the window id


float win_width;
float win_height;
int move_space=0;
int ball_w=20;
int ball_h=20;
int ball_r=20;
int ball_speed=400;
int block_w;
int block_h;
int ballX;
int ballY;
float padX;
float padY;
int blockX;
int blockY;
int pad_length;
stringstream ss;
char input;
string s;
long int current;
long int period;
int n;
int Pause_d;
int score=0;
int topscore=0;
char green[] = "#00FF00";

class Ball;
class Block;
class Paddle;
class BeginDirection;

float flip(float direction, char d);

struct XInfo {
    Display  *display;
    Window   window;
    int             width;          // size of window
    int             height;
    int 		screen;
    Block		***block;
    int 		Row_Block;
    int 		Col_Block;
    Ball 		*ball;
    Paddle		*paddle;
    GC		gc[4];
    BeginDirection *bdirection;
    bool 		begin;
    bool 		gameover;
    Pixmap		pixmap;
    bool 		stop;
    int 		FPS;
    bool  		menu;
};


/*
 * An abstract class representing displayable things.
 */
class Displayable {
public:
    virtual void paint(XInfo &xinfo) = 0;
};

class Block : public Displayable {
public:
    Block(int x, int y, int r, int c):x(x),y(y),r(r),c(c){};
    virtual void paint(XInfo &xInfo){
        XFillRectangle(xInfo.display, xInfo.pixmap, xInfo.gc[3],
                       x+2,y+2,
                       block_w-4,
                       block_h-4);
    }
    int getX(){
        return x;}
    void changeX(int a){
        x=a;}
    void changeY(int b){
        y=b;}
    int getY(){
        return y;}
    ~Block(){};
private:
    int x;
    int y;
    int r;
    int c;
};


class Ball : public Displayable {
public:
    Ball(int x, int y, int rad_w, int rad_h):x(x),y(y),w(rad_w), h(rad_h){};
    virtual void paint(XInfo &xInfo){
        XFillArc(xInfo.display, xInfo.pixmap, xInfo.gc[1], x, y, ball_w, ball_h, 0, 360*64);}
    void setSpeed(int s, float  angle){
        speed=s;
        direction = angle;
    }
    void changespeed(int n){
        speed=n;}
    void changedirection(int n){
        direction=n;}
    void setW(int n){
        w=n;}
    void setH(int n){
        h=n;}
    int getW(){
        return w;}
    int getH(){
        return h;}
    void move(int a, int b){
        x=a;
        y=b;
    }
    void moving(int FPS){
        float nextX=x+(this->speed/FPS)*cos(M_PI*this->direction/180);
        float nextY=y-(this->speed/FPS)*sin(M_PI*this->direction/180);
        if(x > 0 && x+ball_w<win_width && y>0){
            x=nextX;
            y=nextY;}
        else if(x+ball_w>=win_width){
            x=win_width-ball_w-1;
            direction=flip(direction,'r');}
        else if(x<=0){
            x=1;
            direction=flip(direction,'l');}
        else if(y<=0){
            y=1;
            direction=flip(direction,'d');}
        else{
        }
    }
    int getX(){
        return x;}
    int getY(){
        return y;}
    int getdirection(){
        return direction;}
private:
    int x;
    int y;
    int w;
    int h;
    float direction;
    float speed;
};

class Paddle : public Displayable {
public:
    Paddle(int x, int y,int length):x(x),y(y),length(length){};
    virtual void paint(XInfo &xInfo){
        XFillRectangle(xInfo.display, xInfo.pixmap, xInfo.gc[1],
                       x,
                       y,
                       length,
                       win_height*0.007);}
    
    void move(int a, int b){
        if(a> 0 && a+pad_length < win_width){
            x=a;
            y=b;}
        else if( a <= 0){
            x=0;}
        else if(a+pad_length >= win_width){
            x=win_width-pad_length;
        }
    }
    void changelength(float n){
        length=n;}
    float getLength(){
        return length;}
    float getX(){
        return x;}
    float getY(){
        return y;}
    
private:
    int x;
    int y;
    float length;
};


class BeginDirection : public Displayable {
public:
    BeginDirection(float angle, int x, int y, int clockwise):angle(angle), x(x), y(y), clockwise(clockwise){};
    virtual void paint(XInfo &xInfo){
        float x2,y2;
        if(angle<=90 && angle>0){
            x2=x+150*cos(M_PI*angle/180);
            y2=y-150*sin(M_PI*angle/180);
        }
        else{
            x2=x+150*cos(M_PI*angle/180);
            y2=y-150*sin(M_PI*angle/180);}
        XDrawLine(xInfo.display, xInfo.pixmap, xInfo.gc[1],x,y,x2,y2);}
    void rotation(float s){
        if(clockwise){
            if(angle+s<=160){
                angle=angle+s;}
            else{
                clockwise=false;}}
        else{
            if(angle-s>=20){
                angle=angle-s;}
            else
                clockwise=true;}
    }
    void move(int a, int b){
        x=a;
        y=b;}
    int getX(){
        return x;}
    int getY(){
        return y;}
    float getangle(){
        return angle;}
private:
    float angle;
    int x;
    int y;
    bool clockwise;
    
    
    
    
};
// get microseconds
unsigned long now() {
    timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000000 + tv.tv_usec;
}

void setAllBlock( XInfo &xInfo){
    int x=0;
    int y=0;
    for(int row = 0; row<xInfo.Row_Block; row++){
        y=row*block_h;
        for(int col = 0; col<xInfo.Col_Block; col++){
            x=col*block_w;
            xInfo.block[row][col]= new Block(x,y,row,col);}
        x=0;
    }
}

void setPaddle( XInfo &xInfo){
    pad_length=xInfo.width/5;
    padX=xInfo.width*0.4;
    padY=xInfo.height*0.9;
    xInfo.paddle=new Paddle(xInfo.width*0.4, xInfo.height*0.9,pad_length);
    xInfo.paddle->paint(xInfo);
}

void setBeginDirection( XInfo &xInfo ){
    xInfo.bdirection=new BeginDirection(45,xInfo.width*0.5, xInfo.height*0.9-ball_h/2, true);
    xInfo.bdirection->paint(xInfo);
}

void setBall( XInfo &xInfo){
    xInfo.ball=new Ball(xInfo.width*0.5-ball_w/2, xInfo.height*0.9-ball_h/2, ball_w, ball_h);
    //xInfo.ball->paint(xInfo);
    
    
}

float flip(float direction, char d){
    int a=0;
    if(d=='l'||d=='r'){
        if((direction>0 && direction<90)||(direction>=90 && direction<=180)){
            a=180-direction;}
        else{
            a=180+360-direction;}}
    else {
        a=360-direction;
    }
    return a;
}

bool hit_check(int bx, int by, int blockX, int blockY, float direction, XInfo & xInfo){
    if(direction>=0&&direction<90){
        if((bx+ball_w >= blockX && bx+ball_w< blockX + block_w) &&
           (by+ball_h/2 <=blockY+block_h && by+ball_h/2 >= blockY )){
            cout<<"1move right hit to left of block"<<endl;
            xInfo.ball->changedirection(flip(direction, 'r'));
            return true;}//right hit to left of block
        else if((by <= blockY+block_h && by > blockY)&&
                (bx+ball_w/2<=blockX+block_w && bx+ball_w/2>= blockX )){
            xInfo.ball->changedirection(flip(direction, 'd'));
            cout<<"2top hit to bottow of block"<<endl;
            return true;}//move top hit to bottow of block
    }
    else if(direction>=90&&direction<180){
        if((bx >= blockX && bx <  blockX+block_w) &&
           (by+ball_h/2<=blockY+block_h && by+ball_h/2>= blockY )){
            xInfo.ball->changedirection(flip(direction, 'l'));
            cout<<"3move left hit to right of block"<<endl;
            return true;}//left hit to right of block
        else if((by <= blockY+block_h && by > blockY)&&
                (bx+ball_w/2<=blockX+block_w && bx+ball_w/2>= blockX )){
            xInfo.ball->changedirection(flip(direction, 'd'));
            cout<<"4move top hit to bottow of block"<<endl;
            return true;}//move top hit to bottow of block
    }
    else if(direction>=180&&direction<270){
        if((bx >= blockX && bx <  blockX+block_w) &&
           (by + ball_h/2 <= blockY+block_h && by+ball_h/2>= blockY )){
            xInfo.ball->changedirection(flip(direction, 'l'));
            cout<<"5move left hit to right of block"<<endl;
            return true;}//left hit to right of block
        else if((by+ball_h >= blockY && by+ball_h < blockY+block_h) &&
                (bx+ball_w/2<=blockX+block_w && bx+ball_w/2>= blockX)){
            xInfo.ball->changedirection(flip(direction, 'd'));
            cout<<"6move down hit to top of block"<<endl;
            return true;}// down hit to top of block
    }
    else if(direction>=270&&direction<=360){
        if((bx+ball_w >= blockX && bx+ball_w< blockX + block_w) &&
           (by+ball_h/2 <=blockY+block_h && by+ball_h/2 >= blockY )){
            xInfo.ball->changedirection(flip(direction, 'r'));
            cout<<"7move right hit to left of block"<<endl;
            return true;}//right hit to left of block
        else if((by+ball_h >= blockY && by+ball_h < blockY+block_h) &&
                (bx+ball_w/2<=blockX+block_w && bx+ball_w/2>= blockX)){
            xInfo.ball->changedirection(flip(direction, 'd'));
            cout<<"8move down hit to top of block"<<endl;
            return true;}// down hit to top of block
    }
    else{
        cout<<direction<<endl;
        cerr<<"ERROR!!!! on hit_check Function"<<endl;}
    return false;
    
}


void DrawScore(XInfo &xInfo){
	char s1[]="Score:";	 
	XDrawString(xInfo.display, xInfo.pixmap, xInfo.gc[2], xInfo.width*0, xInfo.height*0.9, s1, strlen(s1));
	char s2[]="Score:";	 
	XDrawString(xInfo.display, xInfo.pixmap, xInfo.gc[2], xInfo.width*0, xInfo.height*0.95, s2, strlen(s2));
	
}
void DrawPause(XInfo &xInfo, int x, int y){
    int d=Pause_d;
    XFillArc(xInfo.display, xInfo.pixmap, xInfo.gc[1], x, y, d, d, 0, 360*64);
    XFillRectangle(xInfo.display, xInfo.pixmap, xInfo.gc[0],
                   x+d*0.20, y+d*0.15,
                   d*0.2,
                   d*0.7);
    XFillRectangle(xInfo.display, xInfo.pixmap, xInfo.gc[0],
                   x+d*0.6, y+d*0.15,
                   d*0.2,
                   d*0.7);
    char A[]= "type: 'FPS' OR 'speed' anything else will continue";
    //XTextItem *text=XTextItem(A,strlen(A),2)
    XDrawString(xInfo.display, xInfo.pixmap, xInfo.gc[2], xInfo.width*0.6, xInfo.height*0.7, A, strlen(A));
    
}

void hit(XInfo &xInfo){
    ballX=xInfo.ball->getX();
    ballY=xInfo.ball->getY();
    int bd=xInfo.ball->getdirection();
    if(bd >= 180 && ((ballY+ball_h) >= xInfo.paddle->getY())&&(ballX+ball_w/2>=xInfo.paddle->getX()) && (ballX+ball_w <= xInfo.paddle->getX()+pad_length)){
        xInfo.ball->changedirection(360-bd);}
    for(int row = xInfo.Row_Block-1; row>=0; row--){
        for(int col = 0; col<xInfo.Col_Block; col++){
            if( xInfo.block[row][col]!= NULL){
                blockX=xInfo.block[row][col]->getX();
                blockY=xInfo.block[row][col]->getY();
                if(hit_check(ballX, ballY,blockX,blockY,bd, xInfo)){
                    xInfo.block[row][col]=NULL;
                    score++;
                }
            }
        }
    }
    if(bd >=180 && ((ballY+ball_h) >= xInfo.height)){
        xInfo.gameover=true;}
    
}



void DrawStart(XInfo &xInfo ){
    XFillRectangle(xInfo.display, xInfo.pixmap, xInfo.gc[1],
                   xInfo.width*0.3,
                   xInfo.height*0.1,
                   xInfo.width*0.6,
                   xInfo.height*0.8);
    XFillRectangle(xInfo.display, xInfo.pixmap, xInfo.gc[0],
                   xInfo.width*0.3+4,
                   xInfo.height*0.1+4,
                   xInfo.width*0.6-8,
                   xInfo.height*0.8-8);
    char a1[]="use mouse or keyborad 'a', 'd' to move the pad";
    XDrawString(xInfo.display, xInfo.pixmap, xInfo.gc[2], xInfo.width*0.4, xInfo.height*0.2, a1, strlen(a1));
    char a2[]="type 'p' during game will pasue game ";
    XDrawString(xInfo.display, xInfo.pixmap, xInfo.gc[2], xInfo.width*0.4, xInfo.height*0.4, a2, strlen(a2));
    char a3[]="when game stop. you can type 'FPS' or 'speed' to change ";
    XDrawString(xInfo.display, xInfo.pixmap, xInfo.gc[2], xInfo.width*0.4, xInfo.height*0.6, a3, strlen(a3));
    char a4[]="type v for start";
    XDrawString(xInfo.display, xInfo.pixmap, xInfo.gc[2], xInfo.width*0.4, xInfo.height*0.8, a4, strlen(a4));
    
    
}
void DrawGameover(XInfo &xInfo ){
    XFillRectangle(xInfo.display, xInfo.pixmap, xInfo.gc[1],
                   xInfo.width*0.2,
                   xInfo.height*0.45,
                   xInfo.width*0.6,
                   xInfo.height*0.1);
    XFillRectangle(xInfo.display, xInfo.pixmap, xInfo.gc[0],
                   xInfo.width*0.2+4,
                   xInfo.height*0.45+4,
                   xInfo.width*0.6-8,
                   xInfo.height*0.1-8);
	if(score>topscore){
		char a1[]="Congularation!!!!!your score is topscore !!!!!!(type 'v' to restart)";
		XDrawString(xInfo.display, xInfo.pixmap, xInfo.gc[2], xInfo.width*0.4, xInfo.height*0.2, a1, strlen(a1));
		topscore=score;    	
	}
	else{
		char a1[]="Gameover!!!!!!!!!!!!!!!!!!!!!!!!(type 'v' to restart)";
		XDrawString(xInfo.display, xInfo.pixmap, xInfo.gc[2], xInfo.width*0.4, xInfo.height*0.2, a1, strlen(a1));
	}
    
}
void handleAnimation(XInfo &xInfo) {
    if(xInfo.ball->getX()>=xInfo.width){
	xInfo.gameover=true;
	}
     else if(xInfo.gameover){}
    else if(xInfo.begin){
        xInfo.ball->moving(xInfo.FPS);
        hit(xInfo);}
    else{
        xInfo.bdirection->rotation(2);}
}

void rePaint(XInfo &xInfo){
    XFillRectangle(xInfo.display, xInfo.pixmap, xInfo.gc[0],0,0,xInfo.width,xInfo.height);
    for(int row = 0; row<xInfo.Row_Block; row++){
        for(int col = 0; col<xInfo.Col_Block; col++){
            if( xInfo.block[row][col]!= NULL){
                xInfo.block[row][col]->paint(xInfo);}}}
    xInfo.paddle->paint(xInfo);
    if(!xInfo.begin){
        xInfo.bdirection->paint(xInfo);}
    if(xInfo.menu){
        DrawStart(xInfo);
    }
    else if(xInfo.stop){
        DrawPause(xInfo, xInfo.width*0.5-xInfo.height*0.2,xInfo.height*0.25);
    }
    if(xInfo.gameover){
	DrawGameover(xInfo);	
	}
    xInfo.ball->paint(xInfo);
    DrawScore(xInfo);
    XCopyArea(xInfo.display,xInfo.pixmap,xInfo.window,xInfo.gc[0],0,0,xInfo.width,xInfo.height,0,0);
    XFlush(xInfo.display);
    
    
}

void handleMotion(XInfo &xInfo, XEvent &event, int inside) {
    if (inside) {
        xInfo.paddle->move(event.xbutton.x-pad_length/2, win_height*0.9);
        if(!xInfo.begin){
            xInfo.ball->move(xInfo.paddle->getX()+pad_length/2-ball_w/2, win_height*0.9-ball_h);
            xInfo.bdirection->move(xInfo.paddle->getX()+pad_length/2, xInfo.height*0.9-ball_h/2);}}
}

void handleResize(XInfo &xInfo, XEvent &event) {
    XConfigureEvent xce = event.xconfigure;
    //"Handling resize  w=%d  h=%d\n", xce.width, xce.height);
    
    if (xce.width != xInfo.width || xce.height != xInfo.height) {
        XFreePixmap(xInfo.display, xInfo.pixmap);
        int depth = DefaultDepth(xInfo.display, DefaultScreen(xInfo.display));
        xInfo.pixmap = XCreatePixmap(xInfo.display, xInfo.window, xce.width, xce.height, depth);
        xInfo.width = xce.width;
        xInfo.height = xce.height;
        /////////////////////////////////////////////////
        block_w=xInfo.width/xInfo.Col_Block;
        block_h=xInfo.height*0.3/xInfo.Row_Block;
        padX=xInfo.width*xInfo.paddle->getX()/xInfo.width;
        padY=xInfo.height*0.9;
        pad_length=xInfo.width/5;
        xInfo.paddle->move(padX,padY);
        xInfo.paddle->changelength(pad_length);
        xInfo.bdirection->move(padX+pad_length/2, padY);
        xInfo.ball->move((xInfo.width*xInfo.ball->getX())/xInfo.width, (xInfo.height*xInfo.ball->getY())/win_height);
        int x=0;
        int y=0;
        for(int row = 0; row<xInfo.Row_Block; row++){
            y=row*block_h;
            for(int col = 0; col<xInfo.Col_Block; col++){
                x=col*block_w;
                if(xInfo.block[row][col]!=NULL){
                    xInfo.block[row][col]->changeX(x);
                    xInfo.block[row][col]->changeY(y);}}
            x=0;
        }
        win_width = xce.width;
        win_height	= xce.height;
    }
}


void handleKeyPress(XInfo &xInfo, XEvent &event) {
    KeySym key;
    char text[10];
    
    /*
     * Exit when 'q' is typed.
     * This is a simplified approach that does NOT use localization.
     */
    int i = XLookupString(
                          (XKeyEvent *)&event,    // the keyboard event
                          text,                                   // buffer when text will be written
                          10,                     // size of the text buffer
                          &key,                                   // workstation-independent key symbol
                          NULL );                                 // pointer to a composeStatus structure (unused)
    if ( i == 1) {
        if (text[0] == 'a'){
            if(xInfo.paddle->getX()-move_space>=0){
                xInfo.paddle->move(xInfo.paddle->getX()-move_space, xInfo.height*0.9);
                if(!xInfo.begin){
                    xInfo.ball->move(xInfo.ball->getX()-move_space, xInfo.height*0.9-ball_h);}
                xInfo.bdirection->move(xInfo.bdirection->getX()-move_space, xInfo.height*0.9-ball_h/2);}}
        else if (text[0] == 'd'){
            if(xInfo.paddle->getX()+xInfo.paddle->getLength()+move_space<=xInfo.width){
                xInfo.paddle->move(xInfo.paddle->getX()+move_space, xInfo.height*0.9);
                if(!xInfo.begin){
                    xInfo.ball->move(xInfo.ball->getX()+move_space, xInfo.height*0.9-ball_h);}
                xInfo.bdirection->move(xInfo.bdirection->getX()+move_space, xInfo.height*0.9-ball_h/2);}}
        else if (text[0] == 'p'){
            xInfo.stop=true;
        }
        else if (text[0] == 'v'){
            if(xInfo.menu){
                xInfo.menu=false;
                cout<<xInfo.bdirection->getangle()<<endl;}
            else if(!xInfo.begin&&!xInfo.gameover){
                xInfo.begin=true;
                xInfo.ball->setSpeed(ball_speed, xInfo.bdirection->getangle());}
        }
        
        else if (text[0] == 'q'){
            exit(-1);}
        
    }
}


void initX (int rowB, int colB, XInfo &xInfo ) {
    xInfo.display = XOpenDisplay("");
    xInfo.FPS=30;
    score=0;
    if (!xInfo.display) {exit(-1);}
    xInfo.Row_Block=rowB;
    xInfo.Col_Block=colB;
    xInfo.begin=false;
    xInfo.stop=false;
    xInfo.menu=true;
    xInfo.gameover=false;
    xInfo.screen = DefaultScreen(xInfo.display);
    xInfo.width = DisplayWidth(xInfo.display,xInfo.screen)*0.9;
    win_width = xInfo.width;
    move_space=xInfo.width*0.1;
    xInfo.height = DisplayHeight(xInfo.display, xInfo.screen)*0.9;
    win_height=xInfo.height ;
    // info about the display
   	xInfo.window = XCreateSimpleWindow(
                                       xInfo.display,
                                       XDefaultRootWindow(xInfo.display),				// window's parent
                                       0, 0,			// location: x,y
                                       xInfo.width, xInfo.height,  			// size: width, height
                                       1,								// width of border
                                       XBlackPixel(xInfo.display, xInfo.screen),			// foreground colour
                                       XWhitePixel(xInfo.display, xInfo.screen));			// background colour
    XStoreName(xInfo.display, xInfo.window, "A1");				// save name
    xInfo.pixmap=XCreatePixmap(xInfo.display,xInfo.window, xInfo.width,xInfo.height,DefaultDepth(xInfo.display,xInfo.screen));
    XSelectInput(xInfo.display, xInfo.window,  KeyPressMask);// select events
    //  set graphic content;
    
    cout<<"set Xwindows successed"<<endl;
    Pause_d=xInfo.height*0.4;
    for(int i =0; i<4; i++){
        xInfo.gc[i] = XCreateGC(xInfo.display, xInfo.window, 0, 0);
        XSetForeground(xInfo.display, xInfo.gc[i], BlackPixel(xInfo.display, xInfo.screen));
        XSetBackground(xInfo.display, xInfo.gc[i], WhitePixel(xInfo.display, xInfo.screen));
        XSetFillStyle(xInfo.display, xInfo.gc[i], FillSolid);
        XSetLineAttributes(xInfo.display, xInfo.gc[i],1, LineSolid, CapButt, JoinRound);
    }
    XFontStruct * myfont = XLoadQueryFont (xInfo.display, "6x10");
    XSetFont(xInfo.display, xInfo.gc[2], myfont->fid);
    Colormap colormap= DefaultColormap(xInfo.display, 0);
    XColor green_col;
    XParseColor(xInfo.display, colormap, green, &green_col);
    XAllocColor(xInfo.display, colormap, &green_col);
    XSetForeground(xInfo.display,xInfo.gc[3], green_col.pixel);
    
    
    
    
    
    
    XSelectInput(xInfo.display, xInfo.window, ButtonPressMask | KeyPressMask | PointerMotionMask | EnterWindowMask | LeaveWindowMask |StructureNotifyMask);
    XMapRaised(xInfo.display, xInfo.window);                                // put window on screen
    XSetForeground(xInfo.display, xInfo.gc[0], WhitePixel(xInfo.display, xInfo.screen));
    xInfo.block=new Block ** [xInfo.Row_Block];
    for(int row=0; row<xInfo.Row_Block; row++){
        xInfo.block[row]=new Block * [xInfo.Row_Block];}
    block_w=xInfo.width/xInfo.Col_Block;
    block_h=xInfo.height*0.3/xInfo.Row_Block;
    setAllBlock(xInfo);
    setPaddle(xInfo);
    setBeginDirection(xInfo);
    setBall(xInfo);
    if(!xInfo.begin){
        xInfo.ball->move((xInfo.paddle->getX())+(xInfo.paddle->getLength()/2)
                         -ball_w/2, win_height*0.9-ball_h);}
    XFlush( xInfo.display );							// flush the output buffer
}





void eventLoop(XInfo &xInfo) {
    int inside = 0;
    XEvent event;
    while( true ) {
        if(xInfo.stop){
            string s;
            cout<<"waiting for command....."<<endl;
            cin>>s;
            if(s=="speed"){
                cout<<"what speed you want?"<<endl;
                cin>>ball_speed;
                xInfo.ball->changespeed(ball_speed);}
            else if(s=="FPS"){
                cout<<"what FPS you want?"<<endl;
                cin>>n;
                xInfo.FPS=n;}
            xInfo.stop=false;
        }
        if(xInfo.gameover){
            DrawGameover(xInfo);
        }
        for (int i=0; XPending(xInfo.display) > 0; i++) {
            XNextEvent( xInfo.display, &event );
            //cout << "event.type=" << event.type << "\n";
            switch (event.type) {
                case ButtonPress:                      //
                    cout<<"ButtonPress"<<endl;
                    //handleMotion(xinfo, event, inside);
                    break;
                case MotionNotify:			// mouse movement
                    if(!xInfo.stop){
                        //cout<<"mouse moving"<<endl;
                        handleMotion(xInfo, event, inside);}
                    break;
                case KeyPress:				// any keypress
                    cout<<"KeyPress"<<endl;
                    handleKeyPress(xInfo, event);
                    break;
                case EnterNotify:
                    //cout<<"EnterNotify"<<endl;
                    inside = 1;
                    break;
                case LeaveNotify:
                    //cout<<"LeaveNotify"<<endl;
                    inside = 0;
                    break;
                case ConfigureNotify:
                    if(event.xconfigure.height>300 && event.xconfigure.width>300){
                        handleResize(xInfo, event);}
                    break;
            }
        }
        usleep(1000000/xInfo.FPS);
        handleAnimation(xInfo);
        rePaint(xInfo);
        
    }
}


int main ( int argc, char *argv[] ) {
    int r=10;
    int c=10;
    XInfo xInfo;
    cout<<"begin"<<endl;
    initX(r,c,xInfo);
    cout<<"InitX Successed, with width"<<xInfo.width<<"Height"<<xInfo.width<<endl;
    eventLoop(xInfo);
    cout<<"Eventloop Successed"<<endl;
    XCloseDisplay(xInfo.display);
}


