package com.itextpdf.text.pdf;

import com.itextpdf.text.*;
import com.itextpdf.text.api.Indentable;
import com.itextpdf.text.factories.RomanAlphabetFactory;
import com.itextpdf.text.pdf.interfaces.IAccessibleElement;
import com.itextpdf.text.pdf.parser.*;
import com.itextpdf.text.xml.XMLUtil;
import org.easymock.Capture;
import org.easymock.EasyMockSupport;
import org.easymock.IAnswer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.expect;


public class TaggedPdfTest extends EasyMockSupport {

  /***************************************
   ***************************************
   *         START OF Q4                 *
   ***************************************
   ***************************************/

  @Test
  public void testListAddMutant1() throws IOException, DocumentException, ParserConfigurationException, SAXException {
    // mutant 1
    List list1 = new List(true);
    ListMutant1 list2 = new ListMutant1(true);
    ListItem item1 = mock(ListItem.class);
    final Capture<Chunk> c1 = new Capture<Chunk>();
    item1.setListSymbol(capture(c1));
    expect(item1.getListSymbol()).andAnswer(new IAnswer<Chunk>() {
      public Chunk answer() throws Throwable {
        return c1.getValue();
      }
    }).times(2);
    item1.setIndentationLeft(list1.getSymbolIndent(), list1.isAutoindent());
    item1.setIndentationRight(0.0f);
    ListItem item2 = mock(ListItem.class);
    item2.setListSymbol(list2.getSymbol());
    expect(item2.getListSymbol()).andReturn(list2.getSymbol()).times(2);
    item2.setIndentationLeft(list2.getSymbolIndent(), list2.isAutoindent());
    item2.setIndentationRight(0.0f);
    replayAll();
    list1.add(item1);
    list2.add(item2);
    Assert.assertEquals(item1.getListSymbol().toString(), "1. ");
    Assert.assertEquals(item2.getListSymbol().toString(), "- ");
    Assert.assertFalse(item1.getListSymbol().toString().equals(item2.getListSymbol().toString()));
    verifyAll();
  }

  @Test
  public void testListAddMutant2() throws IOException, DocumentException, ParserConfigurationException, SAXException {
    // mutant 2
    List list1 = new List(true);
    ListMutant2 list2 = new ListMutant2(true);
    list1.add(new ListItem(new Chunk("")));
    list2.add(new ListItem(new Chunk("")));

    ListItem item1 = mock(ListItem.class);
    Capture<Chunk> c1 = new Capture<Chunk>();
    item1.setListSymbol(capture(c1));
    item1.setIndentationLeft(list1.getSymbolIndent(), list1.isAutoindent());
    item1.setIndentationRight(0.0f);
    ListItem item2 = mock(ListItem.class);
    Capture<Chunk> c2 = new Capture<Chunk>();
    item2.setListSymbol(capture(c2));
    item2.setIndentationLeft(list2.getSymbolIndent(), list2.isAutoindent());
    item2.setIndentationRight(0.0f);
    replayAll();
    list1.add(item1);
    list2.add(item2);
    Assert.assertEquals(c1.getValue().toString(), "2. ");
    Assert.assertEquals(c2.getValue().toString(), "1. ");
    Assert.assertFalse(c1.getValue().toString().equals(c2.getValue().toString()));
    verifyAll();
  }

  @Test
  public void testListAddMutant3() throws IOException, DocumentException, ParserConfigurationException, SAXException {
    // mutant 3
    List list1 = new List(true);
    ListMutant3 list2 = new ListMutant3(true);
    list1.add(new ListItem(new Chunk("")));
    list2.add(new ListItem(new Chunk("")));

    ListItem item1 = mock(ListItem.class);
    Capture<Chunk> c1 = new Capture<Chunk>();
    item1.setListSymbol(capture(c1));
    item1.setIndentationLeft(list1.getSymbolIndent(), list1.isAutoindent());
    item1.setIndentationRight(0.0f);
    ListItem item2 = mock(ListItem.class);
    Capture<Chunk> c2 = new Capture<Chunk>();
    item2.setListSymbol(capture(c2));
    item2.setIndentationLeft(list2.getSymbolIndent(), list2.isAutoindent());
    item2.setIndentationRight(0.0f);
    expect(item2.size()).andReturn(100);
    replayAll();
    list1.add(item1);
    list2.add(item2);
    Assert.assertEquals(c1.getValue().toString(), "2. ");
    Assert.assertEquals(c2.getValue().toString(), "101. ");
    Assert.assertFalse(c1.getValue().toString().equals(c2.getValue().toString()));
    verifyAll();
  }

  @Test
  public void testListAddMutant4() throws IOException, DocumentException, ParserConfigurationException, SAXException {
    // mutant 4
    List list1 = new List(true);
    ListMutant4 list2 = new ListMutant4(true);

    ListItem item1 = mock(ListItem.class);
    Capture<Chunk> c1 = new Capture<Chunk>();
    item1.setListSymbol(capture(c1));
    item1.setIndentationLeft(list1.getSymbolIndent(), list1.isAutoindent());
    item1.setIndentationRight(0.0f);
    ListItem item2 = mock(ListItem.class);
    Capture<Chunk> c2 = new Capture<Chunk>();
    item2.setListSymbol(capture(c2));
    item2.setIndentationLeft(list2.getSymbolIndent(), list2.isAutoindent());
    item2.setIndentationRight(0.0f);
    expect(item2.add(item2)).andReturn(true);
    replayAll();
    list1.add(item1);
    list2.add(item2);
    Assert.assertEquals(list1.size(), 1);
    Assert.assertEquals(list2.size(), 0);
    Assert.assertFalse(list1.size() == list2.size());
    verifyAll();
  }

  @Test
  public void testListAddMutant5() throws IOException, DocumentException, ParserConfigurationException, SAXException {
    // mutant 5
    List list1 = new List(true);
    ListMutant5 list2 = new ListMutant5(true);

    List item1 = mock(List.class);
    ListMutant5 item2 = mock(ListMutant5.class);
    expect(item1.getIndentationLeft()).andReturn(0.0f);
    item1.setIndentationLeft(0.0f + list1.getSymbolIndent());
    expect(item2.getIndentationLeft()).andReturn(0.0f);
    item2.setIndentationLeft(0.0f + list2.getSymbolIndent());
    replayAll();
    list1.add(item1);
    list2.add(item2);
    Assert.assertNotNull(list1.getItems().get(0));
    Assert.assertNull(list2.getItems().get(0));
    Assert.assertFalse(list1.getItems().get(0).equals(list2.getItems().get(0)));
    verifyAll();
  }
  /***************************************
   ***************************************
   *           END OF Q4                 *
   ***************************************
   ***************************************/

  private Document document;
  private PdfWriter writer;
  private Paragraph h1;

  private static final String text = new String("Lorem ipsum dolor sit amet," +
      "consectetur adipiscing elit." +
      "Pellentesque a lectus sit amet lectus accumsan aliquam." +
      "Quisque facilisis ullamcorper dolor, quis gravida leo faucibus in. Donec a dolor ligula, quis placerat nunc. Etiam enim velit, egestas in lacinia at, ultricies eu massa." +
      "Cras ornare felis id quam vehicula lobortis. Ut semper malesuada nulla, in vulputate dui eleifend at. Phasellus pulvinar nisl a lorem volutpat pellentesque. In vitae" +
      "ligula et quam vestibulum iaculis eget vitae massa. Fusce vitae leo ut diam suscipit dictum in id sapien. Praesent mi ligula, auctor vitae ultrices in, venenatis non" +
      "odio. Nullam sit amet velit pellentesque lectus consectetur lacinia nec quis mi. In hac habitasse platea dictumst." +
      "Quisque facilisis ullamcorper dolor, quis gravida leo faucibus in." +
      "Donec a dolor ligula, quis placerat nunc.\n" +
      "1. Etiam enim velit, egestas in lacinia at, ultricies eu massa. Cras ornare felis id quam vehicula lobortis. Ut semper malesuada nulla, in vulputate dui eleifend at." +
      "Phasellus pulvinar nisl a lorem volutpat pellentesque. In vitae ligula et quam vestibulum iaculis eget vitae massa. Fusce vitae leo ut diam suscipit dictum in id" +
      "sapien. Praesent mi ligula, auctor vitae ultrices in, venenatis non odio. Nullam sit amet velit pellentesque lectus consectetur lacinia nec quis mi. In hac" +
      "habitasse platea dictumst.\n" +
      "2. Morbi euismod, nunc quis malesuada feugiat, dui nibh rhoncus leo, quis cursus erat tellus vel tortor. Mauris nibh dolor, iaculis et pharetra pretium," +
      "pellentesque vitae erat. Aenean enim nisi, euismod quis ultricies vel, convallis nec nulla. Suspendisse nisl purus, molestie et egestas ac, cursus in mauris." +
      "Aliquam erat volutpat. Donec at nulla in elit faucibus mollis ac vel enim. Nullam dapibus dui sit amet sem consectetur ac vulputate est sagittis. Aliquam luctus" +
      "ornare nulla. Mauris adipiscing congue pharetra. Proin tempus, nibh sed pretium tempor, arcu est hendrerit est, et dignissim odio leo non purus." +
      "Suspendisse non elit massa. Vestibulum tincidunt ipsum vitae dui congue sagittis. Aenean porttitor tristique euismod. Nulla id justo in quam imperdiet" +
      "facilisis ut non turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n" +
      "3. Aliquam non elit ligula, nec hendrerit urna. Mauris ut velit sapien. Sed in convallis diam. Nulla faucibus, purus a porttitor ultrices, est quam convallis magna," +
      "molestie aliquam sapien nulla eget metus. Integer nec enim mi, eu mattis massa. Integer quis sapien vel purus pretium ullamcorper ac id dui. Suspendisse" +
      "pellentesque tellus sit amet neque pulvinar egestas lacinia diam imperdiet.\n" +
      "4. Curabitur hendrerit, sem et facilisis vestibulum, massa felis vestibulum ligula, ut faucibus massa nisi in neque. Nulla facilisi. Etiam diam mauris, pellentesque" +
      "lacinia dapibus at, lobortis non quam. Nullam et neque quis diam vestibulum scelerisque ullamcorper non mauris. Cras massa enim, commodo malesuada" +
      "tincidunt ac, lobortis eu erat. Sed sed risus velit. Suspendisse tellus tortor, ullamcorper nec tristique ac, semper non nulla. Maecenas vitae diam orci, sed" +
      "fermentum enim. Curabitur a libero nisl, vel laoreet nulla. Integer id volutpat sem. Pellentesque blandit, tellus at consequat dictum, urna sem elementum nisi," +
      "a bibendum nisi ipsum sit amet felis. Donec mattis ipsum nec metus lobortis eget volutpat nisl volutpat.\n" +
      "5. Fusce in aliquet nibh. Etiam quis varius ipsum. Vivamus sit amet mauris a libero iaculis semper in a neque. Nam faucibus congue posuere. Cras vitae nibh" +
      "sed magna ultricies pretium. Proin eget lacus quis dui ullamcorper cursus commodo in lacus. Quisque et sem id leo venenatis dictum dignissim et felis." +
      "Vestibulum enim urna, vehicula vel dictum in, congue quis sapien. Quisque ac mauris tellus. Nulla cursus pellentesque mauris viverra bibendum. Fusce" +
      "molestie dui id sem blandit in convallis justo euismod. Curabitur velit nisi, adipiscing sed consequat et, dignissim eget dolor. Aenean malesuada quam id mi" +
      "vestibulum pulvinar. Nullam diam quam, lobortis sit amet semper vitae, tempus eget dolor.");

  private void initializeDocument(String path) throws DocumentException, FileNotFoundException {
    initializeDocument(path, PdfWriter.VERSION_1_7);
  }

  private void initializeDocument(String path, char pdfVersion) throws DocumentException, FileNotFoundException {
    new File("./target/com/itextpdf/test/pdf/TaggedPdfTest/").mkdirs();
    Document.compress = false;
    document = new Document();
    writer = PdfWriter.getInstance(document, new FileOutputStream(path));
    writer.setPdfVersion(pdfVersion);
    writer.setTagged();
    document.open();

    //Required for PDF/UA
    writer.setViewerPreferences(PdfWriter.DisplayDocTitle);
    document.addLanguage("en-US");
    document.addTitle("Some title");
    writer.createXmpMetadata();
    Chunk c = new Chunk("Document Header", new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD, BaseColor.BLUE));
    h1 = new Paragraph(c);
    h1.setRole(PdfName.H1);
  }

  @Test
  public void createTaggedPdf0() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out0.pdf");
    Paragraph paragraph = new Paragraph();
    Chunk c = new Chunk(" Hello ");
    paragraph.add(c);
    c = new Chunk("  world\n\n");
    paragraph.add(c);
    ColumnText columnText = new ColumnText(writer.getDirectContent());
    columnText.setSimpleColumn(36, 36, 250, 800);
    columnText.addElement(paragraph);
    columnText.go();
    document.close();

    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out0.pdf");
    paragraph = new Paragraph();
    c = new Chunk("  ");
    paragraph.add(c);
    columnText = new ColumnText(writer.getDirectContent());
    columnText.setSimpleColumn(36, 36, 250, 800);
    columnText.addElement(paragraph);
    columnText.go();
    document.close();

    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out0.pdf");
    paragraph = new Paragraph();
    c = new Chunk("Hello World");
    paragraph.add(c);
    columnText = new ColumnText(writer.getDirectContent());
    columnText.setSimpleColumn(36, 36, 250, 800);
    columnText.addElement(paragraph);
    columnText.go();
    document.close();

    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out0.pdf");
    paragraph = new Paragraph();
    c = new Chunk("Hello World");
    paragraph.add(c);
    document.add(paragraph);
    document.close();

    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out0.pdf");
    paragraph = new Paragraph();
    c = new Chunk(" Hello ");
    paragraph.add(c);
    c = new Chunk("  world\n");
    paragraph.add(c);
    paragraph.setFont(new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.RED));
    document.add(paragraph);
    document.close();

  }


  @Test
  public void createTaggedPdf1() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out1.pdf");
    Paragraph paragraph = new Paragraph(text);
    paragraph.setFont(new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.RED));
    ColumnText columnText = new ColumnText(writer.getDirectContent());
    columnText.setSimpleColumn(36, 36, 250, 800);
    columnText.addElement(h1);
    columnText.addElement(paragraph);
    columnText.go();
    columnText.setSimpleColumn(300, 36, 500, 800);
    columnText.go();
    document.close();
    compareResults("1");
  }

  @Test
  public void createTaggedPdf2() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out2.pdf");
    Paragraph paragraph = new Paragraph(text);
    ColumnText columnText = new ColumnText(writer.getDirectContent());

    columnText.setSimpleColumn(36, 36, 400, 800);
    columnText.addElement(h1);
    columnText.addElement(paragraph);
    columnText.go();
    document.newPage();
    columnText.setSimpleColumn(36, 36, 400, 800);
    columnText.go();
    document.close();
    compareResults("2");
  }

  @Test
  public void createTaggedPdf3() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out3.pdf");
    Paragraph paragraph = new Paragraph(text);
    document.add(h1);
    document.add(paragraph);
    document.close();
    compareResults("3");
  }

  @Test
  public void createTaggedPdf4() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out4.pdf");
    Paragraph p = new Paragraph();
    PdfName nParagraph = new PdfName("Paragraph");
    p.setRole(nParagraph);
    writer.getStructureTreeRoot().mapRole(nParagraph, PdfName.P);

    try {
      Chunk c = new Chunk("Quick brown ");
      PdfName nTextBlock = new PdfName("TextBlock");
      c.setRole(nTextBlock);
      writer.getStructureTreeRoot().mapRole(nTextBlock, PdfName.SPAN);
      p.add(c);
      Image i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/fox.bmp");
      c = new Chunk(i, 0, 0);
      PdfName nImage = new PdfName("Image");
      c.setRole(nImage);
      writer.getStructureTreeRoot().mapRole(nImage, PdfName.FIGURE);
      c.setAccessibleAttribute(PdfName.ALT, new PdfString("Fox image"));
      p.add(c);
      p.add(new Chunk(" jumped over a lazy "));
      i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/dog.bmp");
      c = new Chunk(i, 0, 0);
      c.setAccessibleAttribute(PdfName.ALT, new PdfString("Dog image"));
      p.add(c);

    } catch (Exception e) {

    }
    document.add(h1);
    document.add(p);
    document.close();
    compareResults("4");
  }

  @Test
  public void createTaggedPdf5() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out5.pdf");
    List list = new List(true);
    try {
      list = new List(true);
      ListItem listItem = new ListItem(new Chunk("Quick brown fox jumped over a lazy dog. A very long line appears here because we need new line."));
      list.add(listItem);
      Image i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/fox.bmp");
      Chunk c = new Chunk(i, 0, 0);
      c.setAccessibleAttribute(PdfName.ALT, new PdfString("Fox image"));
      listItem = new ListItem(c);
      list.add(listItem);
      listItem = new ListItem(new Chunk("jumped over a lazy"));
      list.add(listItem);
      i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/dog.bmp");
      c = new Chunk(i, 0, 0);
      c.setAccessibleAttribute(PdfName.ALT, new PdfString("Dog image"));
      listItem = new ListItem(c);
      list.add(listItem);
    } catch (Exception e) {

    }
    document.add(h1);
    document.add(list);
    document.close();

    compareResults("5");
  }

  @Test
  public void createTaggedPdf6() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out6.pdf");

    ColumnText columnText = new ColumnText(writer.getDirectContent());

    List list = new List(true);
    try {
      list = new List(true);
      ListItem listItem = new ListItem(new Chunk("Quick brown fox jumped over a lazy dog. A very long line appears here because we need new line."));
      list.add(listItem);
      Image i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/fox.bmp");
      Chunk c = new Chunk(i, 0, 0);
      c.setAccessibleAttribute(PdfName.ALT, new PdfString("Fox image"));
      listItem = new ListItem(c);
      list.add(listItem);
      listItem = new ListItem(new Chunk("jumped over a lazy"));
      list.add(listItem);
      i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/dog.bmp");
      c = new Chunk(i, 0, 0);
      c.setAccessibleAttribute(PdfName.ALT, new PdfString("Dog image"));
      listItem = new ListItem(c);
      list.add(listItem);
    } catch (Exception e) {

    }
    columnText.setSimpleColumn(36, 36, 400, 800);
    columnText.addElement(h1);
    columnText.addElement(list);
    columnText.go();
    document.close();
    compareResults("6");
  }

  @Test
  public void createTaggedPdf7() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out7.pdf");
    List list = new List(true);
    try {
      list = new List(true);
      ListItem listItem = new ListItem(new Chunk("Quick brown fox jumped over a lazy dog. A very long line appears here because we need new line."));
      list.add(listItem);
      Image i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/fox.bmp");
      Chunk c = new Chunk(i, 0, 0);
      c.setAccessibleAttribute(PdfName.ALT, new PdfString("Fox image"));
      listItem = new ListItem(c);
      list.add(listItem);
      listItem = new ListItem(new Chunk("jumped over a lazy"));
      list.add(listItem);
      i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/dog.bmp");
      c = new Chunk(i, 0, 0);
      c.setAccessibleAttribute(PdfName.ALT, new PdfString("Dog image"));
      listItem = new ListItem(c);
      list.add(listItem);
      listItem = new ListItem(new Paragraph(text));
      list.add(listItem);
    } catch (Exception e) {

    }
    document.add(h1);
    document.add(list);
    document.close();

    compareResults("7");
  }

  @Test
  public void createTaggedPdf8() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out8.pdf");

    ColumnText columnText = new ColumnText(writer.getDirectContent());

    List list = new List(true);
    try {
      list = new List(true);
      ListItem listItem = new ListItem(new Chunk("Quick brown fox jumped over a lazy dog. A very long line appears here because we need new line."));
      list.add(listItem);
      Image i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/fox.bmp");
      Chunk c = new Chunk(i, 0, 0);
      c.setAccessibleAttribute(PdfName.ALT, new PdfString("Fox image"));
      listItem = new ListItem(c);
      list.add(listItem);
      listItem = new ListItem(new Chunk("jumped over a lazy"));
      list.add(listItem);
      i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/dog.bmp");
      c = new Chunk(i, 0, 0);
      c.setAccessibleAttribute(PdfName.ALT, new PdfString("Dog image"));
      listItem = new ListItem(c);
      list.add(listItem);
      listItem = new ListItem(new Paragraph(text));
      list.add(listItem);
    } catch (Exception e) {

    }
    columnText.setSimpleColumn(36, 36, 400, 800);
    columnText.addElement(h1);
    columnText.addElement(list);
    columnText.go();
    document.newPage();
    columnText.setSimpleColumn(36, 36, 400, 800);
    columnText.go();
    document.close();

    compareResults("8");
  }

  @Test
  public void createTaggedPdf9() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out9.pdf");
    PdfPTable table = new PdfPTable(2);
    try {
      table.addCell("Quick brown fox jumped over a lazy dog. A very long line appears here because we need new line.");
      Image i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/fox.bmp");
      i.setAccessibleAttribute(PdfName.ALT, new PdfString("Fox image"));
      table.addCell(i);
      table.addCell("jumped over a lazy");
      i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/dog.bmp");
      i.setAccessibleAttribute(PdfName.ALT, new PdfString("Dog image"));
      table.addCell(i);
      table.addCell("Hello World");
      Paragraph p = new Paragraph(text);
      table.addCell(p);
    } catch (Exception e) {

    }
    document.add(h1);
    document.add(table);
    document.add(new Paragraph("Extra paragraph at the end of the document. Please make sure that this is really last portion of page content."));
    document.close();
    compareResults("9");
  }

  @Test
  public void createTaggedPdf10() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out10.pdf");
    PdfPTable table = new PdfPTable(2);
    try {
      table.addCell("Quick brown fox jumped over a lazy dog. A very long line appears here because we need new line.");
      Image i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/fox.bmp");
      i.setAccessibleAttribute(PdfName.ALT, new PdfString("Fox image"));
      table.addCell(i);
      table.addCell("jumped over a lazy");
      i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/dog.bmp");
      i.setAccessibleAttribute(PdfName.ALT, new PdfString("Dog image"));
      table.addCell(i);

      PdfPTable t = new PdfPTable(2);
      t.addCell("Quick brown fox jumped over a lazy dog. A very long line appears here because we need new line.");
      i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/fox.bmp");
      i.setAccessibleAttribute(PdfName.ALT, new PdfString("Fox image"));
      t.addCell(i);
      t.addCell("jumped over a lazy");
      i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/dog.bmp");
      i.setAccessibleAttribute(PdfName.ALT, new PdfString("Dog image"));
      t.addCell(i);
      t.addCell(text);
      t.addCell("Hello World");
      table.addCell(t);


      Paragraph p = new Paragraph(text);
      table.addCell(p);
    } catch (Exception e) {

    }
    document.add(h1);
    document.add(table);
    document.close();

    compareResults("10");
  }

  @Test
  public void createTaggedPdf11() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out11.pdf");

    Chapter c = new Chapter(new Paragraph("First chapter", new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.BLUE)), 1);
    c.setTriggerNewPage(false);
    c.setIndentation(40);
    Section s1 = c.addSection(new Paragraph("First section of a first chapter", new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, BaseColor.BLUE)));
    s1.setIndentation(20);
    Section s2 = s1.addSection(new Paragraph("First subsection of a first section of a first chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Second subsection of a first section of a first chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Third subsection of a first section of a first chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s1 = c.addSection(new Paragraph("Second section of a first chapter", new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, BaseColor.BLUE)));
    s1.setIndentation(20);
    s2 = s1.addSection(new Paragraph("First subsection of a second section of a first chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Second subsection of a second section of a first chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Third subsection of a second section of a first chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s1 = c.addSection(new Paragraph("Third section of a first chapter", new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, BaseColor.BLUE)));
    s1.setIndentation(20);
    s2 = s1.addSection(new Paragraph("First subsection of a third section of a first chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Second subsection of a third section of a first chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Third subsection of a third section of a first chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    document.add(c);

    c = new Chapter(new Paragraph("Second chapter", new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.BLUE)), 2);
    c.setTriggerNewPage(false);
    c.setIndentation(40);
    s1 = c.addSection(new Paragraph("First section of a second chapter", new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, BaseColor.BLUE)));
    s1.setIndentation(20);
    s2 = s1.addSection(new Paragraph("First subsection of a first section of a second chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Second subsection of a first section of a second chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Third subsection of a first section of a second chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s1 = c.addSection(new Paragraph("Second section of a second chapter", new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, BaseColor.BLUE)));
    s1.setIndentation(20);
    s2 = s1.addSection(new Paragraph("First subsection of a second section of a second chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Second subsection of a second section of a second chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Third subsection of a second section of a second chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s1 = c.addSection(new Paragraph("Third section of a second chapter", new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, BaseColor.BLUE)));
    s1.setIndentation(20);
    s2 = s1.addSection(new Paragraph("First subsection of a third section of a second chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Second subsection of a third section of a second chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Third subsection of a third section of a second chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    document.add(c);

    c = new Chapter(new Paragraph("Third chapter", new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD, BaseColor.BLUE)), 3);
    c.setTriggerNewPage(false);
    c.setIndentation(40);
    s1 = c.addSection(new Paragraph("First section of a third chapter", new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, BaseColor.BLUE)));
    s1.setIndentation(20);
    s2 = s1.addSection(new Paragraph("First subsection of a first section of a third chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Second subsection of a first section of a third chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Third subsection of a first section of a third chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s1 = c.addSection(new Paragraph("Second section of a third chapter", new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, BaseColor.BLUE)));
    s1.setIndentation(20);
    s2 = s1.addSection(new Paragraph("First subsection of a second section of a third chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Second subsection of a second section of a third chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Third subsection of a second section of a third chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s1 = c.addSection(new Paragraph("Third section of a third chapter", new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD, BaseColor.BLUE)));
    s1.setIndentation(20);
    s2 = s1.addSection(new Paragraph("First subsection of a third section of a third chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Second subsection of a third section of a third chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    s2 = s1.addSection(new Paragraph("Third subsection of a third section of a third chapter", new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD, BaseColor.BLUE)));
    s2.setIndentation(10);
    s2.add(new Paragraph("Some text..."));
    document.add(c);
    c = new Chapter(4);
    c.setTriggerNewPage(false);
    c.setIndentation(40);
    c.addSection("First section of a fourths chapter, the chapter itself is invisible");
    document.add(c);

    document.close();

    compareResults("11");
  }

  @Test
  public void createTaggedPdf12() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out12.pdf");

    PdfPTable table = new PdfPTable(2);
    PdfPCell cell = new PdfPCell(new Paragraph("header 1"));
    cell.setColspan(2);
    table.addCell(cell);
    cell = new PdfPCell(new Paragraph("header 2"));
    cell.setColspan(2);
    table.addCell(cell);
    cell = new PdfPCell(new Paragraph("footer 1"));
    cell.setColspan(2);
    table.addCell(cell);
    cell = new PdfPCell(new Paragraph("footer 2"));
    cell.setColspan(2);
    table.addCell(cell);
    table.setHeaderRows(4);
    table.setFooterRows(2);
    try {
      for (int i = 1; i <= 50; i++) {
        table.addCell("row " + i + ", column 1");
        table.addCell("row " + i + ", column 2");
      }
    } catch (Exception e) {

    }
    document.add(table);
    document.close();

    compareResults("12");
  }

  @Test
  public void createTaggedPdf13() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out13.pdf");

    Paragraph p = new Paragraph();
    Chunk chunk = new Chunk("Please visit ");
    p.add(chunk);

    PdfAction action = new PdfAction("http://itextpdf.com");
    chunk = new Chunk("http://itextpdf.com", new Font(Font.FontFamily.HELVETICA, Font.UNDEFINED, Font.UNDERLINE, BaseColor.BLUE));
    chunk.setAction(action);
    p.add(chunk);
    p.add(new Chunk(" for more details."));
    document.add(p);
    document.close();
    compareResults("13");
  }

  @Test
  public void createTaggedPdf14() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out14.pdf");
    Paragraph paragraph = new Paragraph("Document MUST contain 1 page only!");
    document.newPage();
    ColumnText columnText = new ColumnText(writer.getDirectContent());
    columnText.setSimpleColumn(36, 36, 250, 800);
    columnText.addElement(paragraph);
    columnText.go();
    document.close();
    PdfReader reader = new PdfReader("./target/com/itextpdf/test/pdf/TaggedPdfTest/out14.pdf");
    Assert.assertEquals(1, reader.getNumberOfPages());
  }

  @Test
  public void createTaggedPdf15() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out15.pdf");

    Paragraph p = new Paragraph();
    Chunk chunk = new Chunk("Hello tagged world!");
    chunk.setBackground(new BaseColor(255, 0, 255));
    chunk.setFont(FontFactory.getFont("TimesNewRoman", 20, BaseColor.ORANGE));
    chunk.setUnderline(BaseColor.PINK, 1.2f, 1, 1, 1, 0);
    p.add(chunk);

    document.add(p);
    document.close();
    compareResults("15");
  }

  @Test
  public void createTaggedPdf16() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out16.pdf");

    Paragraph p = new Paragraph();
    Chunk chunk = new Chunk("Hello tagged world!");
    chunk.setBackground(new BaseColor(255, 0, 255));
    chunk.setFont(FontFactory.getFont("TimesNewRoman", 20, BaseColor.ORANGE));
    chunk.setUnderline(BaseColor.PINK, 1.2f, 1, 1, 1, 0);
    p.add(chunk);
    PdfDiv div = new PdfDiv();
    div.addElement(p);
    document.add(div);

    document.add(new Paragraph("This paragraph appears between 2 div blocks"));

    div = new PdfDiv();
    div.addElement(new Paragraph(text));
    document.add(div);


    document.close();
    compareResults("16");
  }

  @Test
  public void createTaggedPdf17() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out17.pdf");

    PdfPTable table = new PdfPTable(2);
    PdfPCell cell = new PdfPCell(new Paragraph("h1"));
    cell.setColspan(2);
    table.addCell(cell);
    cell = new PdfPCell(new Paragraph("h2"));
    cell.setColspan(2);
    table.addCell(cell);
    cell = new PdfPCell(new Paragraph("footer 1"));
    cell.setColspan(2);
    table.addCell(cell);
    cell = new PdfPCell(new Paragraph("footer 2"));
    cell.setColspan(2);
    table.addCell(cell);
    table.setHeaderRows(4);
    table.setFooterRows(2);

    try {
      PdfPHeaderCell headerCell = new PdfPHeaderCell();
      headerCell.setScope(PdfPHeaderCell.ROW);
      headerCell.setPhrase(new Phrase("header1"));
      headerCell.setName("header1");
      table.addCell(headerCell);
      PdfPHeaderCell headerCell2 = new PdfPHeaderCell();
      headerCell2.setScope(PdfPHeaderCell.ROW);
      headerCell2.setPhrase(new Phrase("header2"));
      headerCell2.setName("header2");
      table.addCell(headerCell2);
      cell = new PdfPCell(new Phrase("row 2, column 1"));
      cell.addHeader(headerCell);
      table.addCell(cell);
      cell = new PdfPCell(new Phrase("row 2, column 2"));
      cell.addHeader(headerCell2);
      cell.addHeader(headerCell2);
      table.addCell(cell);
    } catch (Exception e) {
      System.out.println(e.getLocalizedMessage());
    }
    document.add(table);
    document.close();
    compareResults("17");
  }

  @Test
  public void createTaggedPdf18() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out18.pdf");

    PdfDiv div = new PdfDiv();

    Paragraph paragraph = new Paragraph(text);
    paragraph.setFont(new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.RED));

    div.setBackgroundColor(BaseColor.MAGENTA);
    div.setTextAlignment(Element.ALIGN_CENTER);
    div.addElement(paragraph);
    document.add(div);
    document.close();
    compareResults("18");
  }

  @Test
  public void createTaggedPdf19() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out19.pdf");

    PdfDiv div = new PdfDiv();
    writer.getDirectContent().openMCBlock(div);

    PdfArtifact artifact = new PdfArtifact();
    artifact.setType(new PdfString("Background"));
    writer.getDirectContent().openMCBlock(artifact);
    writer.getDirectContent().setColorFill(BaseColor.RED);
    writer.getDirectContent().rectangle(100, 100, 400, 400);
    writer.getDirectContent().fill();
    writer.getDirectContent().closeMCBlock(artifact);

    writer.getDirectContent().closeMCBlock(div);

    document.close();
    compareResults("19");
  }

  @Test
  public void createTaggedPdf20() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out20.pdf");

    Paragraph paragraph = new Paragraph();
    paragraph.getFont().setColor(BaseColor.RED);
    Chunk c = new Chunk("Hello ");
    paragraph.add(c);
    c = new Chunk("  world\n\n");
    paragraph.add(c);

    ColumnText columnText = new ColumnText(writer.getDirectContent());
    columnText.setSimpleColumn(36, 36, 250, 800);
    columnText.addElement(paragraph);
    columnText.go();

    PdfTemplate template = writer.getDirectContent().createTemplate(PageSize.A4.getWidth(), PageSize.A4.getHeight());
    writer.getDirectContent().addTemplate(template, 0, 0, true);

    columnText = new ColumnText(template);
    columnText.setSimpleColumn(36, 36, 250, 750);
    columnText.addText(new Phrase(new Chunk("Hello word \n")));
    columnText.go();

    document.newPage();

    paragraph = new Paragraph();
    paragraph.getFont().setColor(BaseColor.RED);
    c = new Chunk("Hello ");
    paragraph.add(c);
    c = new Chunk("  world\n");
    paragraph.add(c);

    columnText = new ColumnText(template);
    columnText.setSimpleColumn(36, 36, 250, 700);
    columnText.addElement(paragraph);
    columnText.go();

    template = writer.getDirectContent().createTemplate(PageSize.A4.getWidth(), PageSize.A4.getHeight());
    writer.getDirectContent().addTemplate(template, 0, 0, true);

    paragraph = new Paragraph();
    paragraph.getFont().setColor(BaseColor.GREEN);
    c = new Chunk("Hello ");
    paragraph.add(c);
    c = new Chunk("  world\n");
    paragraph.add(c);

    columnText = new ColumnText(template);
    columnText.setSimpleColumn(36, 36, 250, 800);
    columnText.addElement(paragraph);
    columnText.go();

    paragraph = new Paragraph();
    paragraph.getFont().setColor(BaseColor.BLUE);
    c = new Chunk("Hello ");
    paragraph.add(c);
    c = new Chunk("  world\n");
    paragraph.add(c);

    template = writer.getDirectContent().createTemplate(PageSize.A4.getWidth(), PageSize.A4.getHeight());

    columnText = new ColumnText(template);
    columnText.setSimpleColumn(36, 36, 250, 650);
    columnText.addElement(paragraph);
    columnText.go();

    writer.getDirectContent().addTemplate(template, 0, 100);

    writer.getDirectContent().addTemplate(template, 0, 50);

    writer.getDirectContent().addTemplate(template, 0, 0);

    document.close();
    compareResults("20");
  }

  @Test
  public void createTaggedPdf21() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    try {
      initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out21.pdf");

      PdfTemplate template = writer.getDirectContent().createTemplate(PageSize.A4.getWidth(), PageSize.A4.getHeight());

      writer.getDirectContent().addTemplate(template, 0, 0, true);

      ColumnText columnText = new ColumnText(template);
      columnText.setSimpleColumn(36, 36, 250, 750);
      columnText.addText(new Phrase("Hello word \n\n"));
      columnText.go();

      document.newPage();
      writer.getDirectContent().addTemplate(template, 0, 0);

      document.close();
    } catch (Exception conformExc) {
      junit.framework.Assert.assertEquals("Template with tagged content could not be used more than once.", conformExc.getMessage());
      return;
    } finally {
      document.close();
    }
    junit.framework.Assert.fail("Expected error: 'Template with tagged content could not be used more than once.");
  }

  @Test
  public void createTaggedPdf22() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out22.pdf", PdfWriter.VERSION_1_4);
    Paragraph p = new Paragraph();
    PdfName nParagraph = new PdfName("Paragraph");
    p.setRole(nParagraph);
    writer.getStructureTreeRoot().mapRole(nParagraph, PdfName.P);

    try {
      Chunk c = new Chunk("Quick brown ");
      PdfName nTextBlock = new PdfName("TextBlock");
      c.setRole(nTextBlock);
      writer.getStructureTreeRoot().mapRole(nTextBlock, PdfName.SPAN);
      p.add(c);
      Image i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/fox.bmp");
      c = new Chunk(i, 0, 0);
      PdfName nImage = new PdfName("Image");
      c.setRole(nImage);
      writer.getStructureTreeRoot().mapRole(nImage, PdfName.FIGURE);
      c.setAccessibleAttribute(PdfName.ALT, new PdfString("Fox image"));
      p.add(c);
      p.add(new Chunk(" jumped over a lazy "));
      i = Image.getInstance("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/dog.bmp");
      c = new Chunk(i, 0, 0);
      c.setAccessibleAttribute(PdfName.ALT, new PdfString("Dog image"));
      p.add(c);

    } catch (Exception e) {

    }
    document.add(h1);
    document.add(p);
    document.close();
    compareResults("22");
  }

  @Test
  public void createTaggedPdf23() throws DocumentException, IOException, ParserConfigurationException, SAXException {
    initializeDocument("./target/com/itextpdf/test/pdf/TaggedPdfTest/out23.pdf", PdfWriter.VERSION_1_4);

    PdfPTable table = new PdfPTable(2);
    PdfPCell cell = new PdfPCell(new Paragraph("header 1"));
    cell.setColspan(2);
    table.addCell(cell);
    cell = new PdfPCell(new Paragraph("header 2"));
    cell.setColspan(2);
    table.addCell(cell);
    cell = new PdfPCell(new Paragraph("footer 1"));
    cell.setColspan(2);
    table.addCell(cell);
    cell = new PdfPCell(new Paragraph("footer 2"));
    cell.setColspan(2);
    table.addCell(cell);
    table.setHeaderRows(4);
    table.setFooterRows(2);
    try {
      for (int i = 1; i <= 50; i++) {
        table.addCell("row " + i + ", column 1");
        table.addCell("row " + i + ", column 2");
      }
    } catch (Exception e) {

    }
    document.add(table);
    document.close();

    compareResults("23");
  }

  @Test
  public void createTaggedPdf26() throws DocumentException, IOException, ParserConfigurationException, SAXException, InterruptedException {
    Document doc = new Document(PageSize.LETTER, 72, 72, 72, 72);
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    PdfWriter writer = PdfWriter.getInstance(doc, baos);
    writer.setTagged();

    doc.open();

    ColumnText ct = new ColumnText(writer.getDirectContent());
    ct.setUseAscender(true);
    ct.setAdjustFirstLine(true);
    ct.setSimpleColumn(doc.left(), doc.bottom(), doc.right(), doc.top());

    Paragraph p = new Paragraph("before");
    ct.addElement(p);

    List list = new List(List.ORDERED, List.NUMERICAL);
    list.setAutoindent(false);
    list.setIndentationLeft(14);
    list.setSymbolIndent(14);

    list.add("Item 1");

    List nested = new List(List.ORDERED, List.NUMERICAL);
    nested.setAutoindent(false);
    nested.setIndentationLeft(14);
    nested.setSymbolIndent(14);
    nested.add("Nested 1");

    list.add(nested);
    ct.addElement(list);

    p = new Paragraph("after");
    ct.addElement(p);

    ct.go();

    doc.close();


    String outPath = "./target/com/itextpdf/test/pdf/TaggedPdfTest/";
    new File(outPath).mkdirs();
    String outFile = outPath + "out26.pdf";
    FileOutputStream fos = new FileOutputStream(new File(outFile));
    fos.write(baos.toByteArray());
    fos.flush();
    fos.close();
    compareResults("26");
  }

  @Test
  public void createTaggedPdf27() throws DocumentException, IOException, ParserConfigurationException, SAXException, InterruptedException {
    Document doc = new Document();
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    PdfWriter writer = PdfWriter.getInstance(doc, baos);
    writer.setTagged();

    doc.open();

    String imagePath = "./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/Desert.jpg";
    Image img = Image.getInstance(imagePath);
    img.setAbsolutePosition(0, 0);
    img.setAccessibleAttribute(PdfName.E, new PdfString("expansion"));
    img.setAccessibleAttribute(PdfName.ALT, new PdfString("alt"));

    PdfTemplate template = writer.getDirectContent().createTemplate(img.getWidth(), img.getHeight());
    writer.getDirectContent().addTemplate(template, 100, 300, true);

    ColumnText ct = new ColumnText(template);
    ct.setSimpleColumn(0, 0, 250, 300);
    ct.addElement(img);
    ct.go();

    doc.close();


    String outPath = "./target/com/itextpdf/test/pdf/TaggedPdfTest/";
    new File(outPath).mkdirs();
    String outFile = outPath + "out27.pdf";
    FileOutputStream fos = new FileOutputStream(new File(outFile));
    fos.write(baos.toByteArray());
    fos.flush();
    fos.close();
    compareResults("27");
  }

  private boolean compareXmls(String xml1, String xml2) throws ParserConfigurationException, SAXException, IOException {
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    dbf.setNamespaceAware(true);
    dbf.setCoalescing(true);
    dbf.setIgnoringElementContentWhitespace(true);
    dbf.setIgnoringComments(true);
    DocumentBuilder db = dbf.newDocumentBuilder();

    org.w3c.dom.Document doc1 = db.parse(new File(xml1));
    doc1.normalizeDocument();

    org.w3c.dom.Document doc2 = db.parse(new File(xml2));
    doc2.normalizeDocument();

    return doc2.isEqualNode(doc1);
  }

  static class MyTaggedPdfReaderTool extends TaggedPdfReaderTool {

    @Override
    public void parseTag(String tag, PdfObject object, PdfDictionary page)
        throws IOException {
      if (object instanceof PdfNumber) {
        PdfNumber mcid = (PdfNumber) object;
        RenderFilter filter = new MyMarkedContentRenderFilter(mcid.intValue());
        TextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
        FilteredTextRenderListener listener = new FilteredTextRenderListener(
            strategy, filter);
        PdfContentStreamProcessor processor = new PdfContentStreamProcessor(
            listener);
        processor.processContent(PdfReader.getPageContent(page), page
            .getAsDict(PdfName.RESOURCES));
        out.print(XMLUtil.escapeXML(listener.getResultantText(), true));
      } else {
        super.parseTag(tag, object, page);
      }
    }

    @Override
    public void inspectChildDictionary(PdfDictionary k) throws IOException {
      inspectChildDictionary(k, true);
    }


  }

  static class MyMarkedContentRenderFilter extends MarkedContentRenderFilter {

    int mcid;

    public MyMarkedContentRenderFilter(int mcid) {
      super(mcid);
      this.mcid = mcid;
    }

    @Override
    public boolean allowText(TextRenderInfo renderInfo) {
      return renderInfo.hasMcid(mcid, true);
    }

  }

  @After
  public void finalize() {
    Document.compress = true;
  }

  private void compareResults(String name) throws IOException, ParserConfigurationException, SAXException {
    PdfReader reader = new PdfReader("./target/com/itextpdf/test/pdf/TaggedPdfTest/out" + name + ".pdf");
    FileOutputStream xmlOut = new FileOutputStream("./target/com/itextpdf/test/pdf/TaggedPdfTest/test" + name + ".xml");
    new MyTaggedPdfReaderTool().convertToXml(reader, xmlOut);
    xmlOut.close();
    Assert.assertTrue(compareXmls("./src/test/resources/com/itextpdf/text/pdf/TaggedPdfTest/test" + name + ".xml", "./target/com/itextpdf/test/pdf/TaggedPdfTest/test" + name + ".xml"));
  }


}

class ListMutant1 implements TextElementArray, Indentable, IAccessibleElement {

  // constants

  /**
   * a possible value for the numbered parameter
   */
  public static final boolean ORDERED = true;
  /**
   * a possible value for the numbered parameter
   */
  public static final boolean UNORDERED = false;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean NUMERICAL = false;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean ALPHABETICAL = true;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean UPPERCASE = false;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean LOWERCASE = true;

  // member variables

  /**
   * This is the <CODE>ArrayList</CODE> containing the different <CODE>ListItem</CODE>s.
   */
  protected ArrayList<Element> list = new ArrayList<Element>();

  /**
   * Indicates if the list has to be numbered.
   */
  protected boolean numbered = false;
  /**
   * Indicates if the listsymbols are numerical or alphabetical.
   */
  protected boolean lettered = false;
  /**
   * Indicates if the listsymbols are lowercase or uppercase.
   */
  protected boolean lowercase = false;
  /**
   * Indicates if the indentation has to be set automatically.
   */
  protected boolean autoindent = false;
  /**
   * Indicates if the indentation of all the items has to be aligned.
   */
  protected boolean alignindent = false;

  /**
   * This variable indicates the first number of a numbered list.
   */
  protected int first = 1;
  /**
   * This is the listsymbol of a list that is not numbered.
   */
  protected Chunk symbol = new Chunk("- ");
  /**
   * In case you are using numbered/lettered lists, this String is added before the number/letter.
   *
   * @since iText 2.1.1
   */
  protected String preSymbol = "";
  /**
   * In case you are using numbered/lettered lists, this String is added after the number/letter.
   *
   * @since iText 2.1.1
   */
  protected String postSymbol = ". ";

  /**
   * The indentation of this list on the left side.
   */
  protected float indentationLeft = 0;
  /**
   * The indentation of this list on the right side.
   */
  protected float indentationRight = 0;
  /**
   * The indentation of the listitems.
   */
  protected float symbolIndent = 0;

  protected PdfName role = PdfName.L;
  protected HashMap<PdfName, PdfObject> accessibleAttributes = null;
  private AccessibleElementId id = null;

  // constructors

  /**
   * Constructs a <CODE>List</CODE>.
   */
  public ListMutant1() {
    this(false, false);
  }

  /**
   * Constructs a <CODE>List</CODE> with a specific symbol indentation.
   *
   * @param  symbolIndent  the symbol indentation
   * @since iText 2.0.8
   */
  public ListMutant1(final float symbolIndent) {
    this.symbolIndent = symbolIndent;
  }

  /**
   * Constructs a <CODE>List</CODE>.
   *
   * @param  numbered    a boolean
   */
  public ListMutant1(final boolean numbered) {
    this(numbered, false);
  }

  /**
   * Constructs a <CODE>List</CODE>.
   *
   * @param lettered has the list to be 'numbered' with letters
   * @param  numbered    a boolean
   */
  public ListMutant1(final boolean numbered, final boolean lettered) {
    this.numbered = numbered;
    this.lettered = lettered;
    this.autoindent = true;
    this.alignindent = true;
  }

  /**
   * Constructs a <CODE>List</CODE>.
   * <p>
   * Remark: the parameter <VAR>symbolIndent</VAR> is important for instance when
   * generating PDF-documents; it indicates the indentation of the listsymbol.
   * It is not important for HTML-documents.
   *
   * @param  numbered    a boolean
   * @param  symbolIndent  the indentation that has to be used for the listsymbol
   */
  public ListMutant1(final boolean numbered, final float symbolIndent) {
    this(numbered, false, symbolIndent);
  }

  /**
   * Creates a list
   *
   * @param numbered     has the list to be numbered?
   * @param lettered     has the list to be 'numbered' with letters
   * @param symbolIndent the indentation of the symbol
   */
  public ListMutant1(final boolean numbered, final boolean lettered, final float symbolIndent) {
    this.numbered = numbered;
    this.lettered = lettered;
    this.symbolIndent = symbolIndent;
  }

  // implementation of the Element-methods

  /**
   * Processes the element by adding it (or the different parts) to an
   * <CODE>ElementListener</CODE>.
   *
   * @param  listener  an <CODE>ElementListener</CODE>
   * @return  <CODE>true</CODE> if the element was processed successfully
   */
  public boolean process(final ElementListener listener) {
    try {
      for (Element element : list) {
        listener.add(element);
      }
      return true;
    } catch (DocumentException de) {
      return false;
    }
  }

  /**
   * Gets the type of the text element.
   *
   * @return a type
   */
  public int type() {
    return Element.LIST;
  }

  /**
   * Gets all the chunks in this element.
   *
   * @return an <CODE>ArrayList</CODE>
   */
  public java.util.List<Chunk> getChunks() {
    java.util.List<Chunk> tmp = new ArrayList<Chunk>();
    for (Element element : list) {
      tmp.addAll(element.getChunks());
    }
    return tmp;
  }

  // methods to set the membervariables

  /**
   * Adds a <CODE>String</CODE> to the <CODE>List</CODE>.
   *
   * @param s the element to add.
   * @return true if adding the object succeeded
   * @since 5.0.1
   */
  public boolean add(final String s) {
    if (s != null) {
      return this.add(new ListItem(s));
    }
    return false;
  }

  /**
   * Adds an <CODE>Element</CODE> to the <CODE>List</CODE>.
   *
   * @return true if adding the object succeeded
   * @param  o    the element to add.
   * @since 5.0.1 (signature changed to use Element)
   */
  public boolean add(final Element o) {
    if (o instanceof ListItem) {
      ListItem item = (ListItem) o;
      if (numbered && lettered) {
        Chunk chunk = new Chunk(preSymbol, symbol.getFont());
        chunk.setAttributes(symbol.getAttributes());
        int index = first + list.size();
        if (lettered)
          chunk.append(RomanAlphabetFactory.getString(index, lowercase));
        else
          chunk.append(String.valueOf(index));
        chunk.append(postSymbol);
        item.setListSymbol(chunk);
      } else {
        item.setListSymbol(symbol);
      }
      item.setIndentationLeft(symbolIndent, autoindent);
      item.setIndentationRight(0);
      return list.add(item);
    } else if (o instanceof ListMutant1) {
      ListMutant1 nested = (ListMutant1) o;
      nested.setIndentationLeft(nested.getIndentationLeft() + symbolIndent);
      first--;
      return list.add(nested);
    }
    return false;
  }

  public ListMutant1 cloneShallow() {
    ListMutant1 clone = new ListMutant1();
    populateProperties(clone);
    return clone;
  }

  protected void populateProperties(ListMutant1 clone) {
    clone.indentationLeft = indentationLeft;
    clone.indentationRight = indentationRight;
    clone.autoindent = autoindent;
    clone.alignindent = alignindent;
    clone.symbolIndent = symbolIndent;
    clone.symbol = symbol;
  }

  // extra methods

  /**
   * Makes sure all the items in the list have the same indentation.
   */
  public void normalizeIndentation() {
    float max = 0;
    for (Element o : list) {
      if (o instanceof ListItem) {
        max = Math.max(max, ((ListItem) o).getIndentationLeft());
      }
    }
    for (Element o : list) {
      if (o instanceof ListItem) {
        ((ListItem) o).setIndentationLeft(max);
      }
    }
  }

  // setters

  /**
   * @param numbered the numbered to set
   */
  public void setNumbered(final boolean numbered) {
    this.numbered = numbered;
  }

  /**
   * @param lettered the lettered to set
   */
  public void setLettered(final boolean lettered) {
    this.lettered = lettered;
  }

  /**
   * @param uppercase the uppercase to set
   */
  public void setLowercase(final boolean uppercase) {
    this.lowercase = uppercase;
  }

  /**
   * @param autoindent the autoindent to set
   */
  public void setAutoindent(final boolean autoindent) {
    this.autoindent = autoindent;
  }

  /**
   * @param alignindent the alignindent to set
   */
  public void setAlignindent(final boolean alignindent) {
    this.alignindent = alignindent;
  }

  /**
   * Sets the number that has to come first in the list.
   *
   * @param  first    a number
   */
  public void setFirst(final int first) {
    this.first = first;
  }

  /**
   * Sets the listsymbol.
   *
   * @param  symbol    a <CODE>Chunk</CODE>
   */
  public void setListSymbol(final Chunk symbol) {
    this.symbol = symbol;
  }

  /**
   * Sets the listsymbol.
   * <p>
   * This is a shortcut for <CODE>setListSymbol(Chunk symbol)</CODE>.
   *
   * @param  symbol    a <CODE>String</CODE>
   */
  public void setListSymbol(final String symbol) {
    this.symbol = new Chunk(symbol);
  }

  /**
   * Sets the indentation of this paragraph on the left side.
   *
   * @param  indentation    the new indentation
   */
  public void setIndentationLeft(final float indentation) {
    this.indentationLeft = indentation;
  }

  /**
   * Sets the indentation of this paragraph on the right side.
   *
   * @param  indentation    the new indentation
   */
  public void setIndentationRight(final float indentation) {
    this.indentationRight = indentation;
  }

  /**
   * @param symbolIndent the symbolIndent to set
   */
  public void setSymbolIndent(final float symbolIndent) {
    this.symbolIndent = symbolIndent;
  }

  // methods to retrieve information

  /**
   * Gets all the items in the list.
   *
   * @return an <CODE>ArrayList</CODE> containing <CODE>ListItem</CODE>s.
   */
  public ArrayList<Element> getItems() {
    return list;
  }

  /**
   * Gets the size of the list.
   *
   * @return a <CODE>size</CODE>
   */
  public int size() {
    return list.size();
  }

  /**
   * Returns <CODE>true</CODE> if the list is empty.
   *
   * @return <CODE>true</CODE> if the list is empty
   */
  public boolean isEmpty() {
    return list.isEmpty();
  }

  /**
   * Gets the leading of the first listitem.
   *
   * @return a <CODE>leading</CODE>
   */
  public float getTotalLeading() {
    if (list.size() < 1) {
      return -1;
    }
    ListItem item = (ListItem) list.get(0);
    return item.getTotalLeading();
  }

  // getters

  /**
   * Checks if the list is numbered.
   *
   * @return  <CODE>true</CODE> if the list is numbered, <CODE>false</CODE> otherwise.
   */

  public boolean isNumbered() {
    return numbered;
  }

  /**
   * Checks if the list is lettered.
   *
   * @return <CODE>true</CODE> if the list is lettered, <CODE>false</CODE> otherwise.
   */
  public boolean isLettered() {
    return lettered;
  }

  /**
   * Checks if the list lettering is lowercase.
   *
   * @return <CODE>true</CODE> if it is lowercase, <CODE>false</CODE> otherwise.
   */
  public boolean isLowercase() {
    return lowercase;
  }

  /**
   * Checks if the indentation of list items is done automatically.
   *
   * @return the autoindent
   */
  public boolean isAutoindent() {
    return autoindent;
  }

  /**
   * Checks if all the listitems should be aligned.
   *
   * @return the alignindent
   */
  public boolean isAlignindent() {
    return alignindent;
  }

  /**
   * Gets the first number        .
   *
   * @return a number
   */
  public int getFirst() {
    return first;
  }

  /**
   * Gets the Chunk containing the symbol.
   *
   * @return a Chunk with a symbol
   */
  public Chunk getSymbol() {
    return symbol;
  }

  /**
   * Gets the indentation of this paragraph on the left side.
   *
   * @return the indentation
   */
  public float getIndentationLeft() {
    return indentationLeft;
  }

  /**
   * Gets the indentation of this paragraph on the right side.
   *
   * @return the indentation
   */
  public float getIndentationRight() {
    return indentationRight;
  }

  /**
   * Gets the symbol indentation.
   *
   * @return the symbol indentation
   */
  public float getSymbolIndent() {
    return symbolIndent;
  }

  /**
   * @since iText 2.0.8
   * @see Element#isContent()
   */
  public boolean isContent() {
    return true;
  }

  /**
   * @since iText 2.0.8
   * @see Element#isNestable()
   */
  public boolean isNestable() {
    return true;
  }

  /**
   * Returns the String that is after a number or letter in the list symbol.
   *
   * @return the String that is after a number or letter in the list symbol
   * @since iText 2.1.1
   */
  public String getPostSymbol() {
    return postSymbol;
  }

  /**
   * Sets the String that has to be added after a number or letter in the list symbol.
   *
   * @since iText 2.1.1
   * @param  postSymbol the String that has to be added after a number or letter in the list symbol.
   */
  public void setPostSymbol(final String postSymbol) {
    this.postSymbol = postSymbol;
  }

  /**
   * Returns the String that is before a number or letter in the list symbol.
   *
   * @return the String that is before a number or letter in the list symbol
   * @since iText 2.1.1
   */
  public String getPreSymbol() {
    return preSymbol;
  }

  /**
   * Sets the String that has to be added before a number or letter in the list symbol.
   *
   * @since iText 2.1.1
   * @param  preSymbol the String that has to be added before a number or letter in the list symbol.
   */
  public void setPreSymbol(final String preSymbol) {
    this.preSymbol = preSymbol;
  }

  public ListItem getFirstItem() {
    Element lastElement = list.size() > 0 ? list.get(0) : null;
    if (lastElement != null) {
      if (lastElement instanceof ListItem) {
        return (ListItem) lastElement;
      } else if (lastElement instanceof ListMutant1) {
        return ((ListMutant1) lastElement).getFirstItem();
      }
    }
    return null;
  }

  public ListItem getLastItem() {
    Element lastElement = list.size() > 0 ? list.get(list.size() - 1) : null;
    if (lastElement != null) {
      if (lastElement instanceof ListItem) {
        return (ListItem) lastElement;
      } else if (lastElement instanceof ListMutant1) {
        return ((ListMutant1) lastElement).getLastItem();
      }
    }
    return null;
  }

  public PdfObject getAccessibleAttribute(final PdfName key) {
    if (accessibleAttributes != null)
      return accessibleAttributes.get(key);
    else
      return null;
  }

  public void setAccessibleAttribute(final PdfName key, final PdfObject value) {
    if (accessibleAttributes == null)
      accessibleAttributes = new HashMap<PdfName, PdfObject>();
    accessibleAttributes.put(key, value);
  }

  public HashMap<PdfName, PdfObject> getAccessibleAttributes() {
    return accessibleAttributes;
  }

  public PdfName getRole() {
    return role;
  }

  public void setRole(final PdfName role) {
    this.role = role;
  }

  public AccessibleElementId getId() {
    if (id == null)
      id = new AccessibleElementId();
    return id;
  }

  public void setId(final AccessibleElementId id) {
    this.id = id;
  }

  public boolean isInline() {
    return false;
  }
}

class ListMutant2 implements TextElementArray, Indentable, IAccessibleElement {

  // constants

  /**
   * a possible value for the numbered parameter
   */
  public static final boolean ORDERED = true;
  /**
   * a possible value for the numbered parameter
   */
  public static final boolean UNORDERED = false;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean NUMERICAL = false;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean ALPHABETICAL = true;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean UPPERCASE = false;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean LOWERCASE = true;

  // member variables

  /**
   * This is the <CODE>ArrayList</CODE> containing the different <CODE>ListItem</CODE>s.
   */
  protected ArrayList<Element> list = new ArrayList<Element>();

  /**
   * Indicates if the list has to be numbered.
   */
  protected boolean numbered = false;
  /**
   * Indicates if the listsymbols are numerical or alphabetical.
   */
  protected boolean lettered = false;
  /**
   * Indicates if the listsymbols are lowercase or uppercase.
   */
  protected boolean lowercase = false;
  /**
   * Indicates if the indentation has to be set automatically.
   */
  protected boolean autoindent = false;
  /**
   * Indicates if the indentation of all the items has to be aligned.
   */
  protected boolean alignindent = false;

  /**
   * This variable indicates the first number of a numbered list.
   */
  protected int first = 1;
  /**
   * This is the listsymbol of a list that is not numbered.
   */
  protected Chunk symbol = new Chunk("- ");
  /**
   * In case you are using numbered/lettered lists, this String is added before the number/letter.
   *
   * @since iText 2.1.1
   */
  protected String preSymbol = "";
  /**
   * In case you are using numbered/lettered lists, this String is added after the number/letter.
   *
   * @since iText 2.1.1
   */
  protected String postSymbol = ". ";

  /**
   * The indentation of this list on the left side.
   */
  protected float indentationLeft = 0;
  /**
   * The indentation of this list on the right side.
   */
  protected float indentationRight = 0;
  /**
   * The indentation of the listitems.
   */
  protected float symbolIndent = 0;

  protected PdfName role = PdfName.L;
  protected HashMap<PdfName, PdfObject> accessibleAttributes = null;
  private AccessibleElementId id = null;

  // constructors

  /**
   * Constructs a <CODE>List</CODE>.
   */
  public ListMutant2() {
    this(false, false);
  }

  /**
   * Constructs a <CODE>List</CODE> with a specific symbol indentation.
   *
   * @param  symbolIndent  the symbol indentation
   * @since iText 2.0.8
   */
  public ListMutant2(final float symbolIndent) {
    this.symbolIndent = symbolIndent;
  }

  /**
   * Constructs a <CODE>List</CODE>.
   *
   * @param  numbered    a boolean
   */
  public ListMutant2(final boolean numbered) {
    this(numbered, false);
  }

  /**
   * Constructs a <CODE>List</CODE>.
   *
   * @param lettered has the list to be 'numbered' with letters
   * @param  numbered    a boolean
   */
  public ListMutant2(final boolean numbered, final boolean lettered) {
    this.numbered = numbered;
    this.lettered = lettered;
    this.autoindent = true;
    this.alignindent = true;
  }

  /**
   * Constructs a <CODE>List</CODE>.
   * <p>
   * Remark: the parameter <VAR>symbolIndent</VAR> is important for instance when
   * generating PDF-documents; it indicates the indentation of the listsymbol.
   * It is not important for HTML-documents.
   *
   * @param  numbered    a boolean
   * @param  symbolIndent  the indentation that has to be used for the listsymbol
   */
  public ListMutant2(final boolean numbered, final float symbolIndent) {
    this(numbered, false, symbolIndent);
  }

  /**
   * Creates a list
   *
   * @param numbered     has the list to be numbered?
   * @param lettered     has the list to be 'numbered' with letters
   * @param symbolIndent the indentation of the symbol
   */
  public ListMutant2(final boolean numbered, final boolean lettered, final float symbolIndent) {
    this.numbered = numbered;
    this.lettered = lettered;
    this.symbolIndent = symbolIndent;
  }

  // implementation of the Element-methods

  /**
   * Processes the element by adding it (or the different parts) to an
   * <CODE>ElementListener</CODE>.
   *
   * @param  listener  an <CODE>ElementListener</CODE>
   * @return  <CODE>true</CODE> if the element was processed successfully
   */
  public boolean process(final ElementListener listener) {
    try {
      for (Element element : list) {
        listener.add(element);
      }
      return true;
    } catch (DocumentException de) {
      return false;
    }
  }

  /**
   * Gets the type of the text element.
   *
   * @return a type
   */
  public int type() {
    return Element.LIST;
  }

  /**
   * Gets all the chunks in this element.
   *
   * @return an <CODE>ArrayList</CODE>
   */
  public java.util.List<Chunk> getChunks() {
    java.util.List<Chunk> tmp = new ArrayList<Chunk>();
    for (Element element : list) {
      tmp.addAll(element.getChunks());
    }
    return tmp;
  }

  // methods to set the membervariables

  /**
   * Adds a <CODE>String</CODE> to the <CODE>List</CODE>.
   *
   * @param s the element to add.
   * @return true if adding the object succeeded
   * @since 5.0.1
   */
  public boolean add(final String s) {
    if (s != null) {
      return this.add(new ListItem(s));
    }
    return false;
  }

  /**
   * Adds an <CODE>Element</CODE> to the <CODE>List</CODE>.
   *
   * @return true if adding the object succeeded
   * @param  o    the element to add.
   * @since 5.0.1 (signature changed to use Element)
   */
  public boolean add(final Element o) {
    if (o instanceof ListItem) {
      ListItem item = (ListItem) o;
      if (numbered || lettered) {
        Chunk chunk = new Chunk(preSymbol, symbol.getFont());
        chunk.setAttributes(symbol.getAttributes());
        int index = first;
        if (lettered)
          chunk.append(RomanAlphabetFactory.getString(index, lowercase));
        else
          chunk.append(String.valueOf(index));
        chunk.append(postSymbol);
        item.setListSymbol(chunk);
      } else {
        item.setListSymbol(symbol);
      }
      item.setIndentationLeft(symbolIndent, autoindent);
      item.setIndentationRight(0);
      return list.add(item);
    } else if (o instanceof ListMutant2) {
      ListMutant2 nested = (ListMutant2) o;
      nested.setIndentationLeft(nested.getIndentationLeft() + symbolIndent);
      first--;
      return list.add(nested);
    }
    return false;
  }

  public ListMutant2 cloneShallow() {
    ListMutant2 clone = new ListMutant2();
    populateProperties(clone);
    return clone;
  }

  protected void populateProperties(ListMutant2 clone) {
    clone.indentationLeft = indentationLeft;
    clone.indentationRight = indentationRight;
    clone.autoindent = autoindent;
    clone.alignindent = alignindent;
    clone.symbolIndent = symbolIndent;
    clone.symbol = symbol;
  }

  // extra methods

  /**
   * Makes sure all the items in the list have the same indentation.
   */
  public void normalizeIndentation() {
    float max = 0;
    for (Element o : list) {
      if (o instanceof ListItem) {
        max = Math.max(max, ((ListItem) o).getIndentationLeft());
      }
    }
    for (Element o : list) {
      if (o instanceof ListItem) {
        ((ListItem) o).setIndentationLeft(max);
      }
    }
  }

  // setters

  /**
   * @param numbered the numbered to set
   */
  public void setNumbered(final boolean numbered) {
    this.numbered = numbered;
  }

  /**
   * @param lettered the lettered to set
   */
  public void setLettered(final boolean lettered) {
    this.lettered = lettered;
  }

  /**
   * @param uppercase the uppercase to set
   */
  public void setLowercase(final boolean uppercase) {
    this.lowercase = uppercase;
  }

  /**
   * @param autoindent the autoindent to set
   */
  public void setAutoindent(final boolean autoindent) {
    this.autoindent = autoindent;
  }

  /**
   * @param alignindent the alignindent to set
   */
  public void setAlignindent(final boolean alignindent) {
    this.alignindent = alignindent;
  }

  /**
   * Sets the number that has to come first in the list.
   *
   * @param  first    a number
   */
  public void setFirst(final int first) {
    this.first = first;
  }

  /**
   * Sets the listsymbol.
   *
   * @param  symbol    a <CODE>Chunk</CODE>
   */
  public void setListSymbol(final Chunk symbol) {
    this.symbol = symbol;
  }

  /**
   * Sets the listsymbol.
   * <p>
   * This is a shortcut for <CODE>setListSymbol(Chunk symbol)</CODE>.
   *
   * @param  symbol    a <CODE>String</CODE>
   */
  public void setListSymbol(final String symbol) {
    this.symbol = new Chunk(symbol);
  }

  /**
   * Sets the indentation of this paragraph on the left side.
   *
   * @param  indentation    the new indentation
   */
  public void setIndentationLeft(final float indentation) {
    this.indentationLeft = indentation;
  }

  /**
   * Sets the indentation of this paragraph on the right side.
   *
   * @param  indentation    the new indentation
   */
  public void setIndentationRight(final float indentation) {
    this.indentationRight = indentation;
  }

  /**
   * @param symbolIndent the symbolIndent to set
   */
  public void setSymbolIndent(final float symbolIndent) {
    this.symbolIndent = symbolIndent;
  }

  // methods to retrieve information

  /**
   * Gets all the items in the list.
   *
   * @return an <CODE>ArrayList</CODE> containing <CODE>ListItem</CODE>s.
   */
  public ArrayList<Element> getItems() {
    return list;
  }

  /**
   * Gets the size of the list.
   *
   * @return a <CODE>size</CODE>
   */
  public int size() {
    return list.size();
  }

  /**
   * Returns <CODE>true</CODE> if the list is empty.
   *
   * @return <CODE>true</CODE> if the list is empty
   */
  public boolean isEmpty() {
    return list.isEmpty();
  }

  /**
   * Gets the leading of the first listitem.
   *
   * @return a <CODE>leading</CODE>
   */
  public float getTotalLeading() {
    if (list.size() < 1) {
      return -1;
    }
    ListItem item = (ListItem) list.get(0);
    return item.getTotalLeading();
  }

  // getters

  /**
   * Checks if the list is numbered.
   *
   * @return  <CODE>true</CODE> if the list is numbered, <CODE>false</CODE> otherwise.
   */

  public boolean isNumbered() {
    return numbered;
  }

  /**
   * Checks if the list is lettered.
   *
   * @return <CODE>true</CODE> if the list is lettered, <CODE>false</CODE> otherwise.
   */
  public boolean isLettered() {
    return lettered;
  }

  /**
   * Checks if the list lettering is lowercase.
   *
   * @return <CODE>true</CODE> if it is lowercase, <CODE>false</CODE> otherwise.
   */
  public boolean isLowercase() {
    return lowercase;
  }

  /**
   * Checks if the indentation of list items is done automatically.
   *
   * @return the autoindent
   */
  public boolean isAutoindent() {
    return autoindent;
  }

  /**
   * Checks if all the listitems should be aligned.
   *
   * @return the alignindent
   */
  public boolean isAlignindent() {
    return alignindent;
  }

  /**
   * Gets the first number        .
   *
   * @return a number
   */
  public int getFirst() {
    return first;
  }

  /**
   * Gets the Chunk containing the symbol.
   *
   * @return a Chunk with a symbol
   */
  public Chunk getSymbol() {
    return symbol;
  }

  /**
   * Gets the indentation of this paragraph on the left side.
   *
   * @return the indentation
   */
  public float getIndentationLeft() {
    return indentationLeft;
  }

  /**
   * Gets the indentation of this paragraph on the right side.
   *
   * @return the indentation
   */
  public float getIndentationRight() {
    return indentationRight;
  }

  /**
   * Gets the symbol indentation.
   *
   * @return the symbol indentation
   */
  public float getSymbolIndent() {
    return symbolIndent;
  }

  /**
   * @since iText 2.0.8
   * @see Element#isContent()
   */
  public boolean isContent() {
    return true;
  }

  /**
   * @since iText 2.0.8
   * @see Element#isNestable()
   */
  public boolean isNestable() {
    return true;
  }

  /**
   * Returns the String that is after a number or letter in the list symbol.
   *
   * @return the String that is after a number or letter in the list symbol
   * @since iText 2.1.1
   */
  public String getPostSymbol() {
    return postSymbol;
  }

  /**
   * Sets the String that has to be added after a number or letter in the list symbol.
   *
   * @since iText 2.1.1
   * @param  postSymbol the String that has to be added after a number or letter in the list symbol.
   */
  public void setPostSymbol(final String postSymbol) {
    this.postSymbol = postSymbol;
  }

  /**
   * Returns the String that is before a number or letter in the list symbol.
   *
   * @return the String that is before a number or letter in the list symbol
   * @since iText 2.1.1
   */
  public String getPreSymbol() {
    return preSymbol;
  }

  /**
   * Sets the String that has to be added before a number or letter in the list symbol.
   *
   * @since iText 2.1.1
   * @param  preSymbol the String that has to be added before a number or letter in the list symbol.
   */
  public void setPreSymbol(final String preSymbol) {
    this.preSymbol = preSymbol;
  }

  public ListItem getFirstItem() {
    Element lastElement = list.size() > 0 ? list.get(0) : null;
    if (lastElement != null) {
      if (lastElement instanceof ListItem) {
        return (ListItem) lastElement;
      } else if (lastElement instanceof ListMutant2) {
        return ((ListMutant2) lastElement).getFirstItem();
      }
    }
    return null;
  }

  public ListItem getLastItem() {
    Element lastElement = list.size() > 0 ? list.get(list.size() - 1) : null;
    if (lastElement != null) {
      if (lastElement instanceof ListItem) {
        return (ListItem) lastElement;
      } else if (lastElement instanceof ListMutant2) {
        return ((ListMutant2) lastElement).getLastItem();
      }
    }
    return null;
  }

  public PdfObject getAccessibleAttribute(final PdfName key) {
    if (accessibleAttributes != null)
      return accessibleAttributes.get(key);
    else
      return null;
  }

  public void setAccessibleAttribute(final PdfName key, final PdfObject value) {
    if (accessibleAttributes == null)
      accessibleAttributes = new HashMap<PdfName, PdfObject>();
    accessibleAttributes.put(key, value);
  }

  public HashMap<PdfName, PdfObject> getAccessibleAttributes() {
    return accessibleAttributes;
  }

  public PdfName getRole() {
    return role;
  }

  public void setRole(final PdfName role) {
    this.role = role;
  }

  public AccessibleElementId getId() {
    if (id == null)
      id = new AccessibleElementId();
    return id;
  }

  public void setId(final AccessibleElementId id) {
    this.id = id;
  }

  public boolean isInline() {
    return false;
  }
}

class ListMutant3 implements TextElementArray, Indentable, IAccessibleElement {

  // constants

  /**
   * a possible value for the numbered parameter
   */
  public static final boolean ORDERED = true;
  /**
   * a possible value for the numbered parameter
   */
  public static final boolean UNORDERED = false;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean NUMERICAL = false;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean ALPHABETICAL = true;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean UPPERCASE = false;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean LOWERCASE = true;

  // member variables

  /**
   * This is the <CODE>ArrayList</CODE> containing the different <CODE>ListItem</CODE>s.
   */
  protected ArrayList<Element> list = new ArrayList<Element>();

  /**
   * Indicates if the list has to be numbered.
   */
  protected boolean numbered = false;
  /**
   * Indicates if the listsymbols are numerical or alphabetical.
   */
  protected boolean lettered = false;
  /**
   * Indicates if the listsymbols are lowercase or uppercase.
   */
  protected boolean lowercase = false;
  /**
   * Indicates if the indentation has to be set automatically.
   */
  protected boolean autoindent = false;
  /**
   * Indicates if the indentation of all the items has to be aligned.
   */
  protected boolean alignindent = false;

  /**
   * This variable indicates the first number of a numbered list.
   */
  protected int first = 1;
  /**
   * This is the listsymbol of a list that is not numbered.
   */
  protected Chunk symbol = new Chunk("- ");
  /**
   * In case you are using numbered/lettered lists, this String is added before the number/letter.
   *
   * @since iText 2.1.1
   */
  protected String preSymbol = "";
  /**
   * In case you are using numbered/lettered lists, this String is added after the number/letter.
   *
   * @since iText 2.1.1
   */
  protected String postSymbol = ". ";

  /**
   * The indentation of this list on the left side.
   */
  protected float indentationLeft = 0;
  /**
   * The indentation of this list on the right side.
   */
  protected float indentationRight = 0;
  /**
   * The indentation of the listitems.
   */
  protected float symbolIndent = 0;

  protected PdfName role = PdfName.L;
  protected HashMap<PdfName, PdfObject> accessibleAttributes = null;
  private AccessibleElementId id = null;

  // constructors

  /**
   * Constructs a <CODE>List</CODE>.
   */
  public ListMutant3() {
    this(false, false);
  }

  /**
   * Constructs a <CODE>List</CODE> with a specific symbol indentation.
   *
   * @param  symbolIndent  the symbol indentation
   * @since iText 2.0.8
   */
  public ListMutant3(final float symbolIndent) {
    this.symbolIndent = symbolIndent;
  }

  /**
   * Constructs a <CODE>List</CODE>.
   *
   * @param  numbered    a boolean
   */
  public ListMutant3(final boolean numbered) {
    this(numbered, false);
  }

  /**
   * Constructs a <CODE>List</CODE>.
   *
   * @param lettered has the list to be 'numbered' with letters
   * @param  numbered    a boolean
   */
  public ListMutant3(final boolean numbered, final boolean lettered) {
    this.numbered = numbered;
    this.lettered = lettered;
    this.autoindent = true;
    this.alignindent = true;
  }

  /**
   * Constructs a <CODE>List</CODE>.
   * <p>
   * Remark: the parameter <VAR>symbolIndent</VAR> is important for instance when
   * generating PDF-documents; it indicates the indentation of the listsymbol.
   * It is not important for HTML-documents.
   *
   * @param  numbered    a boolean
   * @param  symbolIndent  the indentation that has to be used for the listsymbol
   */
  public ListMutant3(final boolean numbered, final float symbolIndent) {
    this(numbered, false, symbolIndent);
  }

  /**
   * Creates a list
   *
   * @param numbered     has the list to be numbered?
   * @param lettered     has the list to be 'numbered' with letters
   * @param symbolIndent the indentation of the symbol
   */
  public ListMutant3(final boolean numbered, final boolean lettered, final float symbolIndent) {
    this.numbered = numbered;
    this.lettered = lettered;
    this.symbolIndent = symbolIndent;
  }

  // implementation of the Element-methods

  /**
   * Processes the element by adding it (or the different parts) to an
   * <CODE>ElementListener</CODE>.
   *
   * @param  listener  an <CODE>ElementListener</CODE>
   * @return  <CODE>true</CODE> if the element was processed successfully
   */
  public boolean process(final ElementListener listener) {
    try {
      for (Element element : list) {
        listener.add(element);
      }
      return true;
    } catch (DocumentException de) {
      return false;
    }
  }

  /**
   * Gets the type of the text element.
   *
   * @return a type
   */
  public int type() {
    return Element.LIST;
  }

  /**
   * Gets all the chunks in this element.
   *
   * @return an <CODE>ArrayList</CODE>
   */
  public java.util.List<Chunk> getChunks() {
    java.util.List<Chunk> tmp = new ArrayList<Chunk>();
    for (Element element : list) {
      tmp.addAll(element.getChunks());
    }
    return tmp;
  }

  // methods to set the membervariables

  /**
   * Adds a <CODE>String</CODE> to the <CODE>List</CODE>.
   *
   * @param s the element to add.
   * @return true if adding the object succeeded
   * @since 5.0.1
   */
  public boolean add(final String s) {
    if (s != null) {
      return this.add(new ListItem(s));
    }
    return false;
  }

  /**
   * Adds an <CODE>Element</CODE> to the <CODE>List</CODE>.
   *
   * @return true if adding the object succeeded
   * @param  o    the element to add.
   * @since 5.0.1 (signature changed to use Element)
   */
  public boolean add(final Element o) {
    if (o instanceof ListItem) {
      ListItem item = (ListItem) o;
      if (numbered || lettered) {
        Chunk chunk = new Chunk(preSymbol, symbol.getFont());
        chunk.setAttributes(symbol.getAttributes());
        int index = first + item.size();
        if (lettered)
          chunk.append(RomanAlphabetFactory.getString(index, lowercase));
        else
          chunk.append(String.valueOf(index));
        chunk.append(postSymbol);
        item.setListSymbol(chunk);
      } else {
        item.setListSymbol(symbol);
      }
      item.setIndentationLeft(symbolIndent, autoindent);
      item.setIndentationRight(0);
      return list.add(item);
    } else if (o instanceof ListMutant3) {
      ListMutant3 nested = (ListMutant3) o;
      nested.setIndentationLeft(nested.getIndentationLeft() + symbolIndent);
      first--;
      return list.add(nested);
    }
    return false;
  }

  public ListMutant3 cloneShallow() {
    ListMutant3 clone = new ListMutant3();
    populateProperties(clone);
    return clone;
  }

  protected void populateProperties(ListMutant3 clone) {
    clone.indentationLeft = indentationLeft;
    clone.indentationRight = indentationRight;
    clone.autoindent = autoindent;
    clone.alignindent = alignindent;
    clone.symbolIndent = symbolIndent;
    clone.symbol = symbol;
  }

  // extra methods

  /**
   * Makes sure all the items in the list have the same indentation.
   */
  public void normalizeIndentation() {
    float max = 0;
    for (Element o : list) {
      if (o instanceof ListItem) {
        max = Math.max(max, ((ListItem) o).getIndentationLeft());
      }
    }
    for (Element o : list) {
      if (o instanceof ListItem) {
        ((ListItem) o).setIndentationLeft(max);
      }
    }
  }

  // setters

  /**
   * @param numbered the numbered to set
   */
  public void setNumbered(final boolean numbered) {
    this.numbered = numbered;
  }

  /**
   * @param lettered the lettered to set
   */
  public void setLettered(final boolean lettered) {
    this.lettered = lettered;
  }

  /**
   * @param uppercase the uppercase to set
   */
  public void setLowercase(final boolean uppercase) {
    this.lowercase = uppercase;
  }

  /**
   * @param autoindent the autoindent to set
   */
  public void setAutoindent(final boolean autoindent) {
    this.autoindent = autoindent;
  }

  /**
   * @param alignindent the alignindent to set
   */
  public void setAlignindent(final boolean alignindent) {
    this.alignindent = alignindent;
  }

  /**
   * Sets the number that has to come first in the list.
   *
   * @param  first    a number
   */
  public void setFirst(final int first) {
    this.first = first;
  }

  /**
   * Sets the listsymbol.
   *
   * @param  symbol    a <CODE>Chunk</CODE>
   */
  public void setListSymbol(final Chunk symbol) {
    this.symbol = symbol;
  }

  /**
   * Sets the listsymbol.
   * <p>
   * This is a shortcut for <CODE>setListSymbol(Chunk symbol)</CODE>.
   *
   * @param  symbol    a <CODE>String</CODE>
   */
  public void setListSymbol(final String symbol) {
    this.symbol = new Chunk(symbol);
  }

  /**
   * Sets the indentation of this paragraph on the left side.
   *
   * @param  indentation    the new indentation
   */
  public void setIndentationLeft(final float indentation) {
    this.indentationLeft = indentation;
  }

  /**
   * Sets the indentation of this paragraph on the right side.
   *
   * @param  indentation    the new indentation
   */
  public void setIndentationRight(final float indentation) {
    this.indentationRight = indentation;
  }

  /**
   * @param symbolIndent the symbolIndent to set
   */
  public void setSymbolIndent(final float symbolIndent) {
    this.symbolIndent = symbolIndent;
  }

  // methods to retrieve information

  /**
   * Gets all the items in the list.
   *
   * @return an <CODE>ArrayList</CODE> containing <CODE>ListItem</CODE>s.
   */
  public ArrayList<Element> getItems() {
    return list;
  }

  /**
   * Gets the size of the list.
   *
   * @return a <CODE>size</CODE>
   */
  public int size() {
    return list.size();
  }

  /**
   * Returns <CODE>true</CODE> if the list is empty.
   *
   * @return <CODE>true</CODE> if the list is empty
   */
  public boolean isEmpty() {
    return list.isEmpty();
  }

  /**
   * Gets the leading of the first listitem.
   *
   * @return a <CODE>leading</CODE>
   */
  public float getTotalLeading() {
    if (list.size() < 1) {
      return -1;
    }
    ListItem item = (ListItem) list.get(0);
    return item.getTotalLeading();
  }

  // getters

  /**
   * Checks if the list is numbered.
   *
   * @return  <CODE>true</CODE> if the list is numbered, <CODE>false</CODE> otherwise.
   */

  public boolean isNumbered() {
    return numbered;
  }

  /**
   * Checks if the list is lettered.
   *
   * @return <CODE>true</CODE> if the list is lettered, <CODE>false</CODE> otherwise.
   */
  public boolean isLettered() {
    return lettered;
  }

  /**
   * Checks if the list lettering is lowercase.
   *
   * @return <CODE>true</CODE> if it is lowercase, <CODE>false</CODE> otherwise.
   */
  public boolean isLowercase() {
    return lowercase;
  }

  /**
   * Checks if the indentation of list items is done automatically.
   *
   * @return the autoindent
   */
  public boolean isAutoindent() {
    return autoindent;
  }

  /**
   * Checks if all the listitems should be aligned.
   *
   * @return the alignindent
   */
  public boolean isAlignindent() {
    return alignindent;
  }

  /**
   * Gets the first number        .
   *
   * @return a number
   */
  public int getFirst() {
    return first;
  }

  /**
   * Gets the Chunk containing the symbol.
   *
   * @return a Chunk with a symbol
   */
  public Chunk getSymbol() {
    return symbol;
  }

  /**
   * Gets the indentation of this paragraph on the left side.
   *
   * @return the indentation
   */
  public float getIndentationLeft() {
    return indentationLeft;
  }

  /**
   * Gets the indentation of this paragraph on the right side.
   *
   * @return the indentation
   */
  public float getIndentationRight() {
    return indentationRight;
  }

  /**
   * Gets the symbol indentation.
   *
   * @return the symbol indentation
   */
  public float getSymbolIndent() {
    return symbolIndent;
  }

  /**
   * @since iText 2.0.8
   * @see Element#isContent()
   */
  public boolean isContent() {
    return true;
  }

  /**
   * @since iText 2.0.8
   * @see Element#isNestable()
   */
  public boolean isNestable() {
    return true;
  }

  /**
   * Returns the String that is after a number or letter in the list symbol.
   *
   * @return the String that is after a number or letter in the list symbol
   * @since iText 2.1.1
   */
  public String getPostSymbol() {
    return postSymbol;
  }

  /**
   * Sets the String that has to be added after a number or letter in the list symbol.
   *
   * @since iText 2.1.1
   * @param  postSymbol the String that has to be added after a number or letter in the list symbol.
   */
  public void setPostSymbol(final String postSymbol) {
    this.postSymbol = postSymbol;
  }

  /**
   * Returns the String that is before a number or letter in the list symbol.
   *
   * @return the String that is before a number or letter in the list symbol
   * @since iText 2.1.1
   */
  public String getPreSymbol() {
    return preSymbol;
  }

  /**
   * Sets the String that has to be added before a number or letter in the list symbol.
   *
   * @since iText 2.1.1
   * @param  preSymbol the String that has to be added before a number or letter in the list symbol.
   */
  public void setPreSymbol(final String preSymbol) {
    this.preSymbol = preSymbol;
  }

  public ListItem getFirstItem() {
    Element lastElement = list.size() > 0 ? list.get(0) : null;
    if (lastElement != null) {
      if (lastElement instanceof ListItem) {
        return (ListItem) lastElement;
      } else if (lastElement instanceof ListMutant3) {
        return ((ListMutant3) lastElement).getFirstItem();
      }
    }
    return null;
  }

  public ListItem getLastItem() {
    Element lastElement = list.size() > 0 ? list.get(list.size() - 1) : null;
    if (lastElement != null) {
      if (lastElement instanceof ListItem) {
        return (ListItem) lastElement;
      } else if (lastElement instanceof ListMutant3) {
        return ((ListMutant3) lastElement).getLastItem();
      }
    }
    return null;
  }

  public PdfObject getAccessibleAttribute(final PdfName key) {
    if (accessibleAttributes != null)
      return accessibleAttributes.get(key);
    else
      return null;
  }

  public void setAccessibleAttribute(final PdfName key, final PdfObject value) {
    if (accessibleAttributes == null)
      accessibleAttributes = new HashMap<PdfName, PdfObject>();
    accessibleAttributes.put(key, value);
  }

  public HashMap<PdfName, PdfObject> getAccessibleAttributes() {
    return accessibleAttributes;
  }

  public PdfName getRole() {
    return role;
  }

  public void setRole(final PdfName role) {
    this.role = role;
  }

  public AccessibleElementId getId() {
    if (id == null)
      id = new AccessibleElementId();
    return id;
  }

  public void setId(final AccessibleElementId id) {
    this.id = id;
  }

  public boolean isInline() {
    return false;
  }
}

class ListMutant4 implements TextElementArray, Indentable, IAccessibleElement {

  // constants

  /**
   * a possible value for the numbered parameter
   */
  public static final boolean ORDERED = true;
  /**
   * a possible value for the numbered parameter
   */
  public static final boolean UNORDERED = false;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean NUMERICAL = false;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean ALPHABETICAL = true;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean UPPERCASE = false;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean LOWERCASE = true;

  // member variables

  /**
   * This is the <CODE>ArrayList</CODE> containing the different <CODE>ListItem</CODE>s.
   */
  protected ArrayList<Element> list = new ArrayList<Element>();

  /**
   * Indicates if the list has to be numbered.
   */
  protected boolean numbered = false;
  /**
   * Indicates if the listsymbols are numerical or alphabetical.
   */
  protected boolean lettered = false;
  /**
   * Indicates if the listsymbols are lowercase or uppercase.
   */
  protected boolean lowercase = false;
  /**
   * Indicates if the indentation has to be set automatically.
   */
  protected boolean autoindent = false;
  /**
   * Indicates if the indentation of all the items has to be aligned.
   */
  protected boolean alignindent = false;

  /**
   * This variable indicates the first number of a numbered list.
   */
  protected int first = 1;
  /**
   * This is the listsymbol of a list that is not numbered.
   */
  protected Chunk symbol = new Chunk("- ");
  /**
   * In case you are using numbered/lettered lists, this String is added before the number/letter.
   *
   * @since iText 2.1.1
   */
  protected String preSymbol = "";
  /**
   * In case you are using numbered/lettered lists, this String is added after the number/letter.
   *
   * @since iText 2.1.1
   */
  protected String postSymbol = ". ";

  /**
   * The indentation of this list on the left side.
   */
  protected float indentationLeft = 0;
  /**
   * The indentation of this list on the right side.
   */
  protected float indentationRight = 0;
  /**
   * The indentation of the listitems.
   */
  protected float symbolIndent = 0;

  protected PdfName role = PdfName.L;
  protected HashMap<PdfName, PdfObject> accessibleAttributes = null;
  private AccessibleElementId id = null;

  // constructors

  /**
   * Constructs a <CODE>List</CODE>.
   */
  public ListMutant4() {
    this(false, false);
  }

  /**
   * Constructs a <CODE>List</CODE> with a specific symbol indentation.
   *
   * @param  symbolIndent  the symbol indentation
   * @since iText 2.0.8
   */
  public ListMutant4(final float symbolIndent) {
    this.symbolIndent = symbolIndent;
  }

  /**
   * Constructs a <CODE>List</CODE>.
   *
   * @param  numbered    a boolean
   */
  public ListMutant4(final boolean numbered) {
    this(numbered, false);
  }

  /**
   * Constructs a <CODE>List</CODE>.
   *
   * @param lettered has the list to be 'numbered' with letters
   * @param  numbered    a boolean
   */
  public ListMutant4(final boolean numbered, final boolean lettered) {
    this.numbered = numbered;
    this.lettered = lettered;
    this.autoindent = true;
    this.alignindent = true;
  }

  /**
   * Constructs a <CODE>List</CODE>.
   * <p>
   * Remark: the parameter <VAR>symbolIndent</VAR> is important for instance when
   * generating PDF-documents; it indicates the indentation of the listsymbol.
   * It is not important for HTML-documents.
   *
   * @param  numbered    a boolean
   * @param  symbolIndent  the indentation that has to be used for the listsymbol
   */
  public ListMutant4(final boolean numbered, final float symbolIndent) {
    this(numbered, false, symbolIndent);
  }

  /**
   * Creates a list
   *
   * @param numbered     has the list to be numbered?
   * @param lettered     has the list to be 'numbered' with letters
   * @param symbolIndent the indentation of the symbol
   */
  public ListMutant4(final boolean numbered, final boolean lettered, final float symbolIndent) {
    this.numbered = numbered;
    this.lettered = lettered;
    this.symbolIndent = symbolIndent;
  }

  // implementation of the Element-methods

  /**
   * Processes the element by adding it (or the different parts) to an
   * <CODE>ElementListener</CODE>.
   *
   * @param  listener  an <CODE>ElementListener</CODE>
   * @return  <CODE>true</CODE> if the element was processed successfully
   */
  public boolean process(final ElementListener listener) {
    try {
      for (Element element : list) {
        listener.add(element);
      }
      return true;
    } catch (DocumentException de) {
      return false;
    }
  }

  /**
   * Gets the type of the text element.
   *
   * @return a type
   */
  public int type() {
    return Element.LIST;
  }

  /**
   * Gets all the chunks in this element.
   *
   * @return an <CODE>ArrayList</CODE>
   */
  public java.util.List<Chunk> getChunks() {
    java.util.List<Chunk> tmp = new ArrayList<Chunk>();
    for (Element element : list) {
      tmp.addAll(element.getChunks());
    }
    return tmp;
  }

  // methods to set the membervariables

  /**
   * Adds a <CODE>String</CODE> to the <CODE>List</CODE>.
   *
   * @param s the element to add.
   * @return true if adding the object succeeded
   * @since 5.0.1
   */
  public boolean add(final String s) {
    if (s != null) {
      return this.add(new ListItem(s));
    }
    return false;
  }

  /**
   * Adds an <CODE>Element</CODE> to the <CODE>List</CODE>.
   *
   * @return true if adding the object succeeded
   * @param  o    the element to add.
   * @since 5.0.1 (signature changed to use Element)
   */
  public boolean add(final Element o) {
    if (o instanceof ListItem) {
      ListItem item = (ListItem) o;
      if (numbered || lettered) {
        Chunk chunk = new Chunk(preSymbol, symbol.getFont());
        chunk.setAttributes(symbol.getAttributes());
        int index = first + list.size();
        if (lettered)
          chunk.append(RomanAlphabetFactory.getString(index, lowercase));
        else
          chunk.append(String.valueOf(index));
        chunk.append(postSymbol);
        item.setListSymbol(chunk);
      } else {
        item.setListSymbol(symbol);
      }
      item.setIndentationLeft(symbolIndent, autoindent);
      item.setIndentationRight(0);
      return item.add(item);
    } else if (o instanceof ListMutant4) {
      ListMutant4 nested = (ListMutant4) o;
      nested.setIndentationLeft(nested.getIndentationLeft() + symbolIndent);
      first--;
      return list.add(nested);
    }
    return false;
  }

  public ListMutant4 cloneShallow() {
    ListMutant4 clone = new ListMutant4();
    populateProperties(clone);
    return clone;
  }

  protected void populateProperties(ListMutant4 clone) {
    clone.indentationLeft = indentationLeft;
    clone.indentationRight = indentationRight;
    clone.autoindent = autoindent;
    clone.alignindent = alignindent;
    clone.symbolIndent = symbolIndent;
    clone.symbol = symbol;
  }

  // extra methods

  /**
   * Makes sure all the items in the list have the same indentation.
   */
  public void normalizeIndentation() {
    float max = 0;
    for (Element o : list) {
      if (o instanceof ListItem) {
        max = Math.max(max, ((ListItem) o).getIndentationLeft());
      }
    }
    for (Element o : list) {
      if (o instanceof ListItem) {
        ((ListItem) o).setIndentationLeft(max);
      }
    }
  }

  // setters

  /**
   * @param numbered the numbered to set
   */
  public void setNumbered(final boolean numbered) {
    this.numbered = numbered;
  }

  /**
   * @param lettered the lettered to set
   */
  public void setLettered(final boolean lettered) {
    this.lettered = lettered;
  }

  /**
   * @param uppercase the uppercase to set
   */
  public void setLowercase(final boolean uppercase) {
    this.lowercase = uppercase;
  }

  /**
   * @param autoindent the autoindent to set
   */
  public void setAutoindent(final boolean autoindent) {
    this.autoindent = autoindent;
  }

  /**
   * @param alignindent the alignindent to set
   */
  public void setAlignindent(final boolean alignindent) {
    this.alignindent = alignindent;
  }

  /**
   * Sets the number that has to come first in the list.
   *
   * @param  first    a number
   */
  public void setFirst(final int first) {
    this.first = first;
  }

  /**
   * Sets the listsymbol.
   *
   * @param  symbol    a <CODE>Chunk</CODE>
   */
  public void setListSymbol(final Chunk symbol) {
    this.symbol = symbol;
  }

  /**
   * Sets the listsymbol.
   * <p>
   * This is a shortcut for <CODE>setListSymbol(Chunk symbol)</CODE>.
   *
   * @param  symbol    a <CODE>String</CODE>
   */
  public void setListSymbol(final String symbol) {
    this.symbol = new Chunk(symbol);
  }

  /**
   * Sets the indentation of this paragraph on the left side.
   *
   * @param  indentation    the new indentation
   */
  public void setIndentationLeft(final float indentation) {
    this.indentationLeft = indentation;
  }

  /**
   * Sets the indentation of this paragraph on the right side.
   *
   * @param  indentation    the new indentation
   */
  public void setIndentationRight(final float indentation) {
    this.indentationRight = indentation;
  }

  /**
   * @param symbolIndent the symbolIndent to set
   */
  public void setSymbolIndent(final float symbolIndent) {
    this.symbolIndent = symbolIndent;
  }

  // methods to retrieve information

  /**
   * Gets all the items in the list.
   *
   * @return an <CODE>ArrayList</CODE> containing <CODE>ListItem</CODE>s.
   */
  public ArrayList<Element> getItems() {
    return list;
  }

  /**
   * Gets the size of the list.
   *
   * @return a <CODE>size</CODE>
   */
  public int size() {
    return list.size();
  }

  /**
   * Returns <CODE>true</CODE> if the list is empty.
   *
   * @return <CODE>true</CODE> if the list is empty
   */
  public boolean isEmpty() {
    return list.isEmpty();
  }

  /**
   * Gets the leading of the first listitem.
   *
   * @return a <CODE>leading</CODE>
   */
  public float getTotalLeading() {
    if (list.size() < 1) {
      return -1;
    }
    ListItem item = (ListItem) list.get(0);
    return item.getTotalLeading();
  }

  // getters

  /**
   * Checks if the list is numbered.
   *
   * @return  <CODE>true</CODE> if the list is numbered, <CODE>false</CODE> otherwise.
   */

  public boolean isNumbered() {
    return numbered;
  }

  /**
   * Checks if the list is lettered.
   *
   * @return <CODE>true</CODE> if the list is lettered, <CODE>false</CODE> otherwise.
   */
  public boolean isLettered() {
    return lettered;
  }

  /**
   * Checks if the list lettering is lowercase.
   *
   * @return <CODE>true</CODE> if it is lowercase, <CODE>false</CODE> otherwise.
   */
  public boolean isLowercase() {
    return lowercase;
  }

  /**
   * Checks if the indentation of list items is done automatically.
   *
   * @return the autoindent
   */
  public boolean isAutoindent() {
    return autoindent;
  }

  /**
   * Checks if all the listitems should be aligned.
   *
   * @return the alignindent
   */
  public boolean isAlignindent() {
    return alignindent;
  }

  /**
   * Gets the first number        .
   *
   * @return a number
   */
  public int getFirst() {
    return first;
  }

  /**
   * Gets the Chunk containing the symbol.
   *
   * @return a Chunk with a symbol
   */
  public Chunk getSymbol() {
    return symbol;
  }

  /**
   * Gets the indentation of this paragraph on the left side.
   *
   * @return the indentation
   */
  public float getIndentationLeft() {
    return indentationLeft;
  }

  /**
   * Gets the indentation of this paragraph on the right side.
   *
   * @return the indentation
   */
  public float getIndentationRight() {
    return indentationRight;
  }

  /**
   * Gets the symbol indentation.
   *
   * @return the symbol indentation
   */
  public float getSymbolIndent() {
    return symbolIndent;
  }

  /**
   * @since iText 2.0.8
   * @see Element#isContent()
   */
  public boolean isContent() {
    return true;
  }

  /**
   * @since iText 2.0.8
   * @see Element#isNestable()
   */
  public boolean isNestable() {
    return true;
  }

  /**
   * Returns the String that is after a number or letter in the list symbol.
   *
   * @return the String that is after a number or letter in the list symbol
   * @since iText 2.1.1
   */
  public String getPostSymbol() {
    return postSymbol;
  }

  /**
   * Sets the String that has to be added after a number or letter in the list symbol.
   *
   * @since iText 2.1.1
   * @param  postSymbol the String that has to be added after a number or letter in the list symbol.
   */
  public void setPostSymbol(final String postSymbol) {
    this.postSymbol = postSymbol;
  }

  /**
   * Returns the String that is before a number or letter in the list symbol.
   *
   * @return the String that is before a number or letter in the list symbol
   * @since iText 2.1.1
   */
  public String getPreSymbol() {
    return preSymbol;
  }

  /**
   * Sets the String that has to be added before a number or letter in the list symbol.
   *
   * @since iText 2.1.1
   * @param  preSymbol the String that has to be added before a number or letter in the list symbol.
   */
  public void setPreSymbol(final String preSymbol) {
    this.preSymbol = preSymbol;
  }

  public ListItem getFirstItem() {
    Element lastElement = list.size() > 0 ? list.get(0) : null;
    if (lastElement != null) {
      if (lastElement instanceof ListItem) {
        return (ListItem) lastElement;
      } else if (lastElement instanceof ListMutant4) {
        return ((ListMutant4) lastElement).getFirstItem();
      }
    }
    return null;
  }

  public ListItem getLastItem() {
    Element lastElement = list.size() > 0 ? list.get(list.size() - 1) : null;
    if (lastElement != null) {
      if (lastElement instanceof ListItem) {
        return (ListItem) lastElement;
      } else if (lastElement instanceof ListMutant4) {
        return ((ListMutant4) lastElement).getLastItem();
      }
    }
    return null;
  }

  public PdfObject getAccessibleAttribute(final PdfName key) {
    if (accessibleAttributes != null)
      return accessibleAttributes.get(key);
    else
      return null;
  }

  public void setAccessibleAttribute(final PdfName key, final PdfObject value) {
    if (accessibleAttributes == null)
      accessibleAttributes = new HashMap<PdfName, PdfObject>();
    accessibleAttributes.put(key, value);
  }

  public HashMap<PdfName, PdfObject> getAccessibleAttributes() {
    return accessibleAttributes;
  }

  public PdfName getRole() {
    return role;
  }

  public void setRole(final PdfName role) {
    this.role = role;
  }

  public AccessibleElementId getId() {
    if (id == null)
      id = new AccessibleElementId();
    return id;
  }

  public void setId(final AccessibleElementId id) {
    this.id = id;
  }

  public boolean isInline() {
    return false;
  }
}

class ListMutant5 implements TextElementArray, Indentable, IAccessibleElement {

  // constants

  /**
   * a possible value for the numbered parameter
   */
  public static final boolean ORDERED = true;
  /**
   * a possible value for the numbered parameter
   */
  public static final boolean UNORDERED = false;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean NUMERICAL = false;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean ALPHABETICAL = true;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean UPPERCASE = false;
  /**
   * a possible value for the lettered parameter
   */
  public static final boolean LOWERCASE = true;

  // member variables

  /**
   * This is the <CODE>ArrayList</CODE> containing the different <CODE>ListItem</CODE>s.
   */
  protected ArrayList<Element> list = new ArrayList<Element>();

  /**
   * Indicates if the list has to be numbered.
   */
  protected boolean numbered = false;
  /**
   * Indicates if the listsymbols are numerical or alphabetical.
   */
  protected boolean lettered = false;
  /**
   * Indicates if the listsymbols are lowercase or uppercase.
   */
  protected boolean lowercase = false;
  /**
   * Indicates if the indentation has to be set automatically.
   */
  protected boolean autoindent = false;
  /**
   * Indicates if the indentation of all the items has to be aligned.
   */
  protected boolean alignindent = false;

  /**
   * This variable indicates the first number of a numbered list.
   */
  protected int first = 1;
  /**
   * This is the listsymbol of a list that is not numbered.
   */
  protected Chunk symbol = new Chunk("- ");
  /**
   * In case you are using numbered/lettered lists, this String is added before the number/letter.
   *
   * @since iText 2.1.1
   */
  protected String preSymbol = "";
  /**
   * In case you are using numbered/lettered lists, this String is added after the number/letter.
   *
   * @since iText 2.1.1
   */
  protected String postSymbol = ". ";

  /**
   * The indentation of this list on the left side.
   */
  protected float indentationLeft = 0;
  /**
   * The indentation of this list on the right side.
   */
  protected float indentationRight = 0;
  /**
   * The indentation of the listitems.
   */
  protected float symbolIndent = 0;

  protected PdfName role = PdfName.L;
  protected HashMap<PdfName, PdfObject> accessibleAttributes = null;
  private AccessibleElementId id = null;

  // constructors

  /**
   * Constructs a <CODE>List</CODE>.
   */
  public ListMutant5() {
    this(false, false);
  }

  /**
   * Constructs a <CODE>List</CODE> with a specific symbol indentation.
   *
   * @param  symbolIndent  the symbol indentation
   * @since iText 2.0.8
   */
  public ListMutant5(final float symbolIndent) {
    this.symbolIndent = symbolIndent;
  }

  /**
   * Constructs a <CODE>List</CODE>.
   *
   * @param  numbered    a boolean
   */
  public ListMutant5(final boolean numbered) {
    this(numbered, false);
  }

  /**
   * Constructs a <CODE>List</CODE>.
   *
   * @param lettered has the list to be 'numbered' with letters
   * @param  numbered    a boolean
   */
  public ListMutant5(final boolean numbered, final boolean lettered) {
    this.numbered = numbered;
    this.lettered = lettered;
    this.autoindent = true;
    this.alignindent = true;
  }

  /**
   * Constructs a <CODE>List</CODE>.
   * <p>
   * Remark: the parameter <VAR>symbolIndent</VAR> is important for instance when
   * generating PDF-documents; it indicates the indentation of the listsymbol.
   * It is not important for HTML-documents.
   *
   * @param  numbered    a boolean
   * @param  symbolIndent  the indentation that has to be used for the listsymbol
   */
  public ListMutant5(final boolean numbered, final float symbolIndent) {
    this(numbered, false, symbolIndent);
  }

  /**
   * Creates a list
   *
   * @param numbered     has the list to be numbered?
   * @param lettered     has the list to be 'numbered' with letters
   * @param symbolIndent the indentation of the symbol
   */
  public ListMutant5(final boolean numbered, final boolean lettered, final float symbolIndent) {
    this.numbered = numbered;
    this.lettered = lettered;
    this.symbolIndent = symbolIndent;
  }

  // implementation of the Element-methods

  /**
   * Processes the element by adding it (or the different parts) to an
   * <CODE>ElementListener</CODE>.
   *
   * @param  listener  an <CODE>ElementListener</CODE>
   * @return  <CODE>true</CODE> if the element was processed successfully
   */
  public boolean process(final ElementListener listener) {
    try {
      for (Element element : list) {
        listener.add(element);
      }
      return true;
    } catch (DocumentException de) {
      return false;
    }
  }

  /**
   * Gets the type of the text element.
   *
   * @return a type
   */
  public int type() {
    return Element.LIST;
  }

  /**
   * Gets all the chunks in this element.
   *
   * @return an <CODE>ArrayList</CODE>
   */
  public java.util.List<Chunk> getChunks() {
    java.util.List<Chunk> tmp = new ArrayList<Chunk>();
    for (Element element : list) {
      tmp.addAll(element.getChunks());
    }
    return tmp;
  }

  // methods to set the membervariables

  /**
   * Adds a <CODE>String</CODE> to the <CODE>List</CODE>.
   *
   * @param s the element to add.
   * @return true if adding the object succeeded
   * @since 5.0.1
   */
  public boolean add(final String s) {
    if (s != null) {
      return this.add(new ListItem(s));
    }
    return false;
  }

  /**
   * Adds an <CODE>Element</CODE> to the <CODE>List</CODE>.
   *
   * @return true if adding the object succeeded
   * @param  o    the element to add.
   * @since 5.0.1 (signature changed to use Element)
   */
  public boolean add(final Element o) {
    if (o instanceof ListItem) {
      ListItem item = (ListItem) o;
      if (numbered || lettered) {
        Chunk chunk = new Chunk(preSymbol, symbol.getFont());
        chunk.setAttributes(symbol.getAttributes());
        int index = first + list.size();
        if (lettered)
          chunk.append(RomanAlphabetFactory.getString(index, lowercase));
        else
          chunk.append(String.valueOf(index));
        chunk.append(postSymbol);
        item.setListSymbol(chunk);
      } else {
        item.setListSymbol(symbol);
      }
      item.setIndentationLeft(symbolIndent, autoindent);
      item.setIndentationRight(0);
      return list.add(item);
    } else if (o instanceof ListMutant5) {
      ListMutant5 nested = (ListMutant5) o;
      nested.setIndentationLeft(nested.getIndentationLeft() + symbolIndent);
      first--;
      return list.add(null);
    }
    return false;
  }

  public ListMutant5 cloneShallow() {
    ListMutant5 clone = new ListMutant5();
    populateProperties(clone);
    return clone;
  }

  protected void populateProperties(ListMutant5 clone) {
    clone.indentationLeft = indentationLeft;
    clone.indentationRight = indentationRight;
    clone.autoindent = autoindent;
    clone.alignindent = alignindent;
    clone.symbolIndent = symbolIndent;
    clone.symbol = symbol;
  }

  // extra methods

  /**
   * Makes sure all the items in the list have the same indentation.
   */
  public void normalizeIndentation() {
    float max = 0;
    for (Element o : list) {
      if (o instanceof ListItem) {
        max = Math.max(max, ((ListItem) o).getIndentationLeft());
      }
    }
    for (Element o : list) {
      if (o instanceof ListItem) {
        ((ListItem) o).setIndentationLeft(max);
      }
    }
  }

  // setters

  /**
   * @param numbered the numbered to set
   */
  public void setNumbered(final boolean numbered) {
    this.numbered = numbered;
  }

  /**
   * @param lettered the lettered to set
   */
  public void setLettered(final boolean lettered) {
    this.lettered = lettered;
  }

  /**
   * @param uppercase the uppercase to set
   */
  public void setLowercase(final boolean uppercase) {
    this.lowercase = uppercase;
  }

  /**
   * @param autoindent the autoindent to set
   */
  public void setAutoindent(final boolean autoindent) {
    this.autoindent = autoindent;
  }

  /**
   * @param alignindent the alignindent to set
   */
  public void setAlignindent(final boolean alignindent) {
    this.alignindent = alignindent;
  }

  /**
   * Sets the number that has to come first in the list.
   *
   * @param  first    a number
   */
  public void setFirst(final int first) {
    this.first = first;
  }

  /**
   * Sets the listsymbol.
   *
   * @param  symbol    a <CODE>Chunk</CODE>
   */
  public void setListSymbol(final Chunk symbol) {
    this.symbol = symbol;
  }

  /**
   * Sets the listsymbol.
   * <p>
   * This is a shortcut for <CODE>setListSymbol(Chunk symbol)</CODE>.
   *
   * @param  symbol    a <CODE>String</CODE>
   */
  public void setListSymbol(final String symbol) {
    this.symbol = new Chunk(symbol);
  }

  /**
   * Sets the indentation of this paragraph on the left side.
   *
   * @param  indentation    the new indentation
   */
  public void setIndentationLeft(final float indentation) {
    this.indentationLeft = indentation;
  }

  /**
   * Sets the indentation of this paragraph on the right side.
   *
   * @param  indentation    the new indentation
   */
  public void setIndentationRight(final float indentation) {
    this.indentationRight = indentation;
  }

  /**
   * @param symbolIndent the symbolIndent to set
   */
  public void setSymbolIndent(final float symbolIndent) {
    this.symbolIndent = symbolIndent;
  }

  // methods to retrieve information

  /**
   * Gets all the items in the list.
   *
   * @return an <CODE>ArrayList</CODE> containing <CODE>ListItem</CODE>s.
   */
  public ArrayList<Element> getItems() {
    return list;
  }

  /**
   * Gets the size of the list.
   *
   * @return a <CODE>size</CODE>
   */
  public int size() {
    return list.size();
  }

  /**
   * Returns <CODE>true</CODE> if the list is empty.
   *
   * @return <CODE>true</CODE> if the list is empty
   */
  public boolean isEmpty() {
    return list.isEmpty();
  }

  /**
   * Gets the leading of the first listitem.
   *
   * @return a <CODE>leading</CODE>
   */
  public float getTotalLeading() {
    if (list.size() < 1) {
      return -1;
    }
    ListItem item = (ListItem) list.get(0);
    return item.getTotalLeading();
  }

  // getters

  /**
   * Checks if the list is numbered.
   *
   * @return  <CODE>true</CODE> if the list is numbered, <CODE>false</CODE> otherwise.
   */

  public boolean isNumbered() {
    return numbered;
  }

  /**
   * Checks if the list is lettered.
   *
   * @return <CODE>true</CODE> if the list is lettered, <CODE>false</CODE> otherwise.
   */
  public boolean isLettered() {
    return lettered;
  }

  /**
   * Checks if the list lettering is lowercase.
   *
   * @return <CODE>true</CODE> if it is lowercase, <CODE>false</CODE> otherwise.
   */
  public boolean isLowercase() {
    return lowercase;
  }

  /**
   * Checks if the indentation of list items is done automatically.
   *
   * @return the autoindent
   */
  public boolean isAutoindent() {
    return autoindent;
  }

  /**
   * Checks if all the listitems should be aligned.
   *
   * @return the alignindent
   */
  public boolean isAlignindent() {
    return alignindent;
  }

  /**
   * Gets the first number        .
   *
   * @return a number
   */
  public int getFirst() {
    return first;
  }

  /**
   * Gets the Chunk containing the symbol.
   *
   * @return a Chunk with a symbol
   */
  public Chunk getSymbol() {
    return symbol;
  }

  /**
   * Gets the indentation of this paragraph on the left side.
   *
   * @return the indentation
   */
  public float getIndentationLeft() {
    return indentationLeft;
  }

  /**
   * Gets the indentation of this paragraph on the right side.
   *
   * @return the indentation
   */
  public float getIndentationRight() {
    return indentationRight;
  }

  /**
   * Gets the symbol indentation.
   *
   * @return the symbol indentation
   */
  public float getSymbolIndent() {
    return symbolIndent;
  }

  /**
   * @since iText 2.0.8
   * @see Element#isContent()
   */
  public boolean isContent() {
    return true;
  }

  /**
   * @since iText 2.0.8
   * @see Element#isNestable()
   */
  public boolean isNestable() {
    return true;
  }

  /**
   * Returns the String that is after a number or letter in the list symbol.
   *
   * @return the String that is after a number or letter in the list symbol
   * @since iText 2.1.1
   */
  public String getPostSymbol() {
    return postSymbol;
  }

  /**
   * Sets the String that has to be added after a number or letter in the list symbol.
   *
   * @since iText 2.1.1
   * @param  postSymbol the String that has to be added after a number or letter in the list symbol.
   */
  public void setPostSymbol(final String postSymbol) {
    this.postSymbol = postSymbol;
  }

  /**
   * Returns the String that is before a number or letter in the list symbol.
   *
   * @return the String that is before a number or letter in the list symbol
   * @since iText 2.1.1
   */
  public String getPreSymbol() {
    return preSymbol;
  }

  /**
   * Sets the String that has to be added before a number or letter in the list symbol.
   *
   * @since iText 2.1.1
   * @param  preSymbol the String that has to be added before a number or letter in the list symbol.
   */
  public void setPreSymbol(final String preSymbol) {
    this.preSymbol = preSymbol;
  }

  public ListItem getFirstItem() {
    Element lastElement = list.size() > 0 ? list.get(0) : null;
    if (lastElement != null) {
      if (lastElement instanceof ListItem) {
        return (ListItem) lastElement;
      } else if (lastElement instanceof ListMutant5) {
        return ((ListMutant5) lastElement).getFirstItem();
      }
    }
    return null;
  }

  public ListItem getLastItem() {
    Element lastElement = list.size() > 0 ? list.get(list.size() - 1) : null;
    if (lastElement != null) {
      if (lastElement instanceof ListItem) {
        return (ListItem) lastElement;
      } else if (lastElement instanceof ListMutant5) {
        return ((ListMutant5) lastElement).getLastItem();
      }
    }
    return null;
  }

  public PdfObject getAccessibleAttribute(final PdfName key) {
    if (accessibleAttributes != null)
      return accessibleAttributes.get(key);
    else
      return null;
  }

  public void setAccessibleAttribute(final PdfName key, final PdfObject value) {
    if (accessibleAttributes == null)
      accessibleAttributes = new HashMap<PdfName, PdfObject>();
    accessibleAttributes.put(key, value);
  }

  public HashMap<PdfName, PdfObject> getAccessibleAttributes() {
    return accessibleAttributes;
  }

  public PdfName getRole() {
    return role;
  }

  public void setRole(final PdfName role) {
    this.role = role;
  }

  public AccessibleElementId getId() {
    if (id == null)
      id = new AccessibleElementId();
    return id;
  }

  public void setId(final AccessibleElementId id) {
    this.id = id;
  }

  public boolean isInline() {
    return false;
  }
}

